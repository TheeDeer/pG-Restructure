#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <morecolors>
#include <basecomm>

#include prestigegaming/defines.sp
#include prestigegaming/admins.sp
#include prestigegaming/statistics.sp
#include prestigegaming/database.sp
#include prestigegaming/adverts.sp
#include prestigegaming/afk.sp
#include prestigegaming/chat.sp
#include prestigegaming/logging.sp
#include prestigegaming/history.sp
#include prestigegaming/reviews.sp
#include prestigegaming/tagenforcer.sp
#include prestigegaming/tips.sp
#include prestigegaming/serverstatus.sp
#include prestigegaming/sessions.sp

public Plugin:myinfo = {
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESC,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

public bool:IsValidClient(client)
{
	if (client < 1 || client > MAXPLAYERS) { return false; }
	if (!IsClientConnected(client)) { return false; }
	if (!IsClientInGame(client)) { return false; }
	if (IsFakeClient(client)) { return false; }
	return true;
}

public OnPluginStart()
{
	GetConVarString(FindConVar("ip"), server_ip, sizeof(server_ip));
	
	ConnectDatabase();
	
	new String:szGameName[30];
	GetGameFolderName(szGameName, sizeof(szGameName));
	if (StrEqual(szGameName, "tf", false))
	{
		game_id = 3;
		HookEvent("player_connect_client", Event_PlayerConnect, EventHookMode_Pre);
		HookEvent("player_changeclass", Event_ChangeClass, EventHookMode_Pre);
		HookEvent("player_team", Event_PlayerTeam, EventHookMode_Pre);
	}
	else if (StrEqual(szGameName, "csgo", false))
	{
		game_id = 2;
	}
	
	HookEvent("player_connect", Event_PlayerConnect, EventHookMode_Pre);
	HookEvent("server_cvar", Event_Cvar, EventHookMode_Pre);
	HookEvent("player_death", OnPlayerDeath, EventHookMode_Post);

	new String:error[512];
	hCache = SQLite_UseDatabase("pg-cache", error, sizeof(error));
	if (hCache == INVALID_HANDLE)
		SetFailState(error);
	
	StartAdmins();
	StartAdverts();
	StartAFK();
	StartEnforcer();
	StartTips();
	StartReviews();
	StartChat();
	StartLogging();
	StartHistory();
	StartServerStatus();
	StartStatistics();
	StartSessions();
}

public OnMapStart()
{
	if (hDatabase != INVALID_HANDLE)
	{
		CloseHandle(hDatabase);
	}
	ConnectDatabase();
	
	mapTime = GetTime();
	CreateTimer(10.0, SaveMapLog);
}

public OnPluginEnd()
{
	CloseHandle(ply_players);
	if (hDatabase != INVALID_HANDLE)
	{
		CloseHandle(hDatabase);
		hDatabase = INVALID_HANDLE;
	}
}

public Action:Event_PlayerConnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	SetEventBroadcast(event, true);
	return Plugin_Continue;
}

public Action:Event_Cvar(Handle:event, const String:name[], bool:dontBroadcast)
{
	SetEventBroadcast(event, true);
	return Plugin_Continue;
}

public Action:Event_ChangeClass(Handle:event, const String:name[], bool:dontBroadcast)
{
	SetEventBroadcast(event, true);
	return Plugin_Continue;
}

public Action:Event_PlayerTeam(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!GetEventBool(event, "silent"))
	{
		SetEventBroadcast(event, true);
	}
	return Plugin_Continue;
}

public Action:OnClientPreAdminCheck(client) {
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	// Call Query with AdminQueryCallback
	new String:query[512];

	SetArrayString(hTags, client, "");
	user_id[client] = 0;

	Format(query, sizeof(query), "SELECT user_id, ply_name, groups FROM pg_admins WHERE ply_id = %s", ply_id);
	SQL_TQuery(hDatabase, AdminQueryCallback, query, client);

	return Plugin_Handled;
}

public OnClientPostAdminCheck(client)
{
	if (!IsValidClient(client)) { return; }
	
	isBanned[client] = false;

	in_name_change[client] = false;
	
	timeAway[client] = 0;
	
	afkPoints[client] = 0;
	
	sessionId[client] = 0;

	CheckUserName(client);

	validateName(client);
	
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	
	new String:ply_ip[24];
	GetClientIP(client, ply_ip, sizeof(ply_ip));
	
	new String:query[1500];
	Format(query, sizeof(query), QRY_BAN[0], ply_id, ply_ip);
	SQL_TQuery(hDatabase, BAN_SELECT, query, client);

	StartUser(client);
}

public OnClientDisconnect(client)
{
	if (!IsValidClient(client)) { return; }
	
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

	new String:ply_id_hr[MAX_STEAMID32_LENGTH];
	GetClientAuthId(client, AuthId_Steam2, ply_id_hr, sizeof(ply_id_hr));
	
	if (isBanned[client] == false)
	{
		PrintToChatAll("%N [%s] disconnected.", client, ply_id_hr);
		
		if(sessionId[client] == 0) { return; }
		
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);
		
		new ply_time = RoundToFloor(GetClientTime(client));
		new String:query[255];
		Format(query, sizeof(query), QRY_LOG[7], server_ip, ply_id, ply_name, sessionId[client], ply_time, timeAway[client]);
		SQL_TQuery(hDatabase, QRY_NONE, query);

		EndSession(client);
	}
	
	isBanned[client] = false;

	in_name_change[client] = false;
	
	timeAway[client] = 0;
	
	afkPoints[client] = 0;
	
	sessionId[client] = 0;
}

public BAN_SELECT(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) { return; }
	
	new target = data;
	if (!IsValidClient(target)) { return; }
	
	new String:target_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, target_id, MAX_COMMUNITYID_LENGTH);
	
	new String:target_ip[16];
	GetClientIP(target, target_ip, sizeof(target_ip));
	
	new String:target_name[36];
	GetClientName(target, target_name, sizeof(target_name));
	
	new ban_length;
	new String:ban_reason[100];
	new ban_remaining;
	if(SQL_FetchRow(hndl))
	{
		do
		{
			new String:target_evasion_id[32];
			SQL_FetchString(hndl,1,target_evasion_id,sizeof(target_evasion_id));
			
			ban_length = SQL_FetchInt(hndl,3);
			
			SQL_FetchString(hndl,4,ban_reason,sizeof(ban_reason));
			
			ban_remaining = SQL_FetchInt(hndl,5);
			
			if (ban_length == 0) { ban_remaining = 0; }
			
			if (ban_remaining >= 0 || ban_length == 0)
			{
				isBanned[target] = true;
				
				if (!StrEqual(target_id, target_evasion_id))
				{
					if (StrEqual(target_evasion_id, "")) { target_evasion_id = target_ip; }
					new String:query[800];
					SQL_EscapeString(hDatabase, target_name, target_name, 255);
					
					Format(query, sizeof(query), QRY_BAN[1], server_ip, target_id, target_name, ban_remaining, target_evasion_id);
					SQL_TQuery(hDatabase, QRY_NONE, query);
				}
				
				new String:duration_string[60];
				GetDurationString(duration_string, sizeof(duration_string), ban_remaining);
				KickClient(target,"Banned (%s) for %s. Contest it on www.prestige-gaming.org", duration_string, ban_reason);
				break;
			}
		}while (SQL_FetchRow(hndl));
	}
	
	if (isBanned[target] == false)
	{
		new String:target_id_hr[MAX_STEAMID32_LENGTH];
		GetClientAuthId(target, AuthId_Steam2, target_id_hr, sizeof(target_id_hr));
		PrintToChatAll("%s [%s] connected.", target_name, target_id_hr);
		
		if(GetConVarInt(pg_statistics) != 0)
		{
			GetClientStats(target_id);
		}
		
		new String:query_join[255];
		SQL_EscapeString(hDatabase, target_name, target_name, 255);
		Format(query_join, sizeof(query_join), QRY_LOG[8], server_ip, target_id, target_name, target_ip);
		SQL_TQuery(hDatabase, OnInsertGetSessionId, query_join, target);
		
		CreateTimer(10.0, GetWelcomeMsg, target, 0);
	}
}

public OnInsertGetSessionId(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) { return; }
	
	new ply = data;
	if (!IsValidClient(ply)) { return; }
	
	sessionId[ply] = SQL_GetInsertId(hndl);
}

public Action:GetWelcomeMsg(Handle:timer, any:client)
{
	if (!IsValidClient(client)) { return; }
	
	CPrintToChat(client, "{green}--------------------");
	CPrintToChat(client, "{lightgreen}Welcome {green}%N {lightgreen}to Prestige Gaming!", client);
	CPrintToChat(client, "{lightgreen}You can visit us on {green}www.prestige-gaming.org", client);
	CPrintToChat(client, "{green}--------------------");
}

GetDurationString(String:duration_string[], duration_string_size, duration)
{
	if (duration == 0)
	{
		strcopy(duration_string, duration_string_size, "permanently");
	}
	else
	{
		Format(duration_string, duration_string_size, "%d %s", duration, (duration == 1) ? "minute" : "minutes");
	}
}

public Action:OnBanClient(client, ban_length, flags, const String:reason[], const String:kick_message[], const String:cmd[], any:admin)
{
	new String:target_id[32];
	GetClientAuthId(client, AuthId_SteamID64, target_id, MAX_COMMUNITYID_LENGTH);
	
	new String:target_ip[16];
	GetClientIP(client, target_ip, sizeof(target_ip));
	
	new String:target_name[36];
	GetClientName(client, target_name, sizeof(target_name));
	SQL_EscapeString(hDatabase, target_name, target_name, 255);
	
	new String:ban_reason[36];
	SQL_EscapeString(hDatabase, reason, ban_reason, 255);
	
	new String:query[800];
	
	if(IsValidClient(admin))
	{
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(admin, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		new String:ply_ip[16];
		GetClientIP(admin, ply_ip, sizeof(ply_ip));
		
		new String:ply_name[36];
		GetAdminUsername(admin, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);

		new ply_admin_id = user_id[admin];
		
		Format(query, sizeof(query), QRY_BAN[3], server_ip, ply_id, ply_admin_id, ply_ip, ply_name, target_id, target_ip, target_name, ban_length, ban_reason);
	}
	else
	{
		Format(query, sizeof(query), QRY_BAN[2], server_ip, target_id, target_ip, target_name, ban_length, ban_reason);
	}
	
	SQL_TQuery(hDatabase, QRY_NONE, query);
	
	new String:duration_string[60];
	GetDurationString(duration_string, sizeof(duration_string), ban_length);
	KickClient(client, "Banned (%s) for %s. Contest it on www.prestige-gaming.org", duration_string, ban_reason);
	
	return Plugin_Stop;
}

public Action:OnClientSayCommand(client, const String:cmd[], const String:sArgs[])
{
	new String:chatMessage[MAX_MESSAGE];
	strcopy(chatMessage, MAX_MESSAGE, sArgs);
	CRemoveTags(chatMessage, sizeof(chatMessage));
	TrimString(chatMessage);
	if (strlen(chatMessage) == 0)
	{
		return Plugin_Stop;
	}
	
	new startidx;
	if (sArgs[startidx] == CHAT_SYMBOL)
	{
		startidx++;
		if (strcmp(cmd, "say", false) == 0)
		{
			if (sArgs[startidx] != CHAT_SYMBOL)
			{
				if (CheckCommandAccess(client, "sm_say", ADMFLAG_CHAT))
				{
					ClientCommand(client, "sm_say %s", sArgs[startidx]);
				}
				else
				{
					ClientCommand(client, "sm_chat %s", sArgs[startidx]);
				}
				return Plugin_Stop;
			}
			
			startidx++;
			if (sArgs[startidx] != CHAT_SYMBOL)
			{
				ClientCommand(client, "sm_psay %s", sArgs[startidx]);
				return Plugin_Stop;
			}
			
			startidx++;
			ClientCommand(client, "sm_csay %s", sArgs[startidx]);
			return Plugin_Stop;
		}
		else if (strcmp(cmd, "say_team", false) == 0)
		{
			ClientCommand(client, "sm_chat %s", sArgs[startidx]);
			return Plugin_Stop;
		}
		return Plugin_Stop;
	}
	
	if (FindCharInString(chatMessage, '/') == 0 || FindCharInString(chatMessage, '!') == 0 || (client != 0 && BaseComm_IsClientGagged(client)))
	{
		return Plugin_Stop;
	}
	
	if(IsValidClient(client))
	{
		new lastMsg = GetTime();
		
		if((lastMsg - antiChatSpam[client]) < 0.75)
		{
			antiChatSpam[client] = lastMsg;
			PrintToChat(client, "[SM] You are flooding the chat!");
			return Plugin_Stop;
		}
		antiChatSpam[client] = lastMsg;
	}
	
	for (new i = 0; i < sizeof(filterWords); i++)
	{
		if (StrContains(sArgs, filterWords[i], false) != -1)
		{
			PrintToChat(client, "[SM] Please be respectful and refrain from using '%s' on our servers.", filterWords[i]);
			return Plugin_Stop;
		}
	}
	
	new String:query[600];
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	if (IsValidClient(client))
	{
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);
		
		Format(query, sizeof(query), QRY_LOG[0], server_ip, ply_id, ply_name, cmd, chatMessage);
		SQL_TQuery(hDatabase, QRY_NONE, query, 0);
	}
	else
	{
		Format(query, sizeof(query), QRY_LOG[1], server_ip, cmd, chatMessage);
		SQL_TQuery(hDatabase, QRY_NONE, query, 0);
		return Plugin_Continue;
	}
	
	new String:name[MAX_MESSAGE];
	GetClientName(client, name, MAX_MESSAGE);
	CRemoveTags(name, sizeof(name));
	TrimString(name);
	Format(name, MAX_MESSAGE, "{teamcolor}%s", name);
	if (strlen(name) == 0)
	{
		return Plugin_Stop;
	}
	new String:format_chat[MAX_MESSAGE] = "%s{default}: %s";
	
	if(GetConVarInt(pg_statistics) != 0)
	{
		new rankInt = GetField(ply_id, "rank");
		new String:rankStr[2];
		IntToString(rankInt, rankStr, sizeof(rankStr));
		if (rankInt >= 1 && rankInt <= 50)
		{
			PrintToServer("rank: %s", rankStr);
			Format(format_chat, MAX_MESSAGE, "{green}[#%s]{teamcolor} %s", rankStr, format_chat);
		}
	}
	
	if (strcmp(cmd, "say_team", false) == 0)
	{
		if (GetConVarInt(sv_teamchat) == 0)
		{
			if(GetConVarInt(sv_teamchat_error))
			{
				CPrintToChatEx(client, client, "{red}Team chat is currently disabled!");
			}
			return Plugin_Handled;
		}
		else
		{
			Format(format_chat, MAX_MESSAGE, "{default}({teamcolor}TEAM{default}) %s", format_chat);
		}
	}
	
	new AdminId:id = GetUserAdmin(client);
	if (id != INVALID_ADMIN_ID)
	{
        if (CheckCommandAccess(client, "somefuckingcommandlolwut", ADMFLAG_SLAY, true))
        {
            ReplaceString(name, MAX_MESSAGE, "[pG]", "{teamcolor}[{red}pG{teamcolor}]", true);
        } else if (CheckCommandAccess(client, "somefuckingcommandlolwut", ADMFLAG_BAN, true))
		{
			ReplaceString(name, MAX_MESSAGE, "[pG]", "[{green}pG{teamcolor}]", true);
		}
	}
	
	// This crashes the server, BUT YOU NEED IT!!!!!!!!!!!!!!
	new Action:fResult;
	Call_StartForward(g_formatChat);
	Call_PushCell(client);
	Call_PushStringEx(chatMessage, MAX_MESSAGE, SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
	Call_Finish(_:fResult);
	
	Call_StartForward(g_formatName);
	Call_PushCell(client);
	Call_PushStringEx(name, MAX_MESSAGE, SM_PARAM_STRING_UTF8 | SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
	Call_Finish(_:fResult);
	
	if (!IsPlayerAlive(client))
	{
		Format(format_chat, MAX_MESSAGE, "{red}*DEAD* %s", format_chat);
	}
	
	Format(format_chat, MAX_MESSAGE, format_chat, name, chatMessage);
	
	for (new to = 1; to < MaxClients; to++)
	{
		if (IsClientInGame(to))
		{
			if ((strcmp(cmd, "say_team", false) == 0 && GetClientTeam(client) == GetClientTeam(to)) || strcmp(cmd, "say_team", false) != 0)
			{
				if (GetConVarInt(sv_deadtalk) == 0 || GetConVarInt(sv_deadtalk_override) == 1)
				{
					new bool:cust1 = false;
					new bool:cust2 = false;
					new AdminId:id2 = GetUserAdmin(to);
					if (id2 != INVALID_ADMIN_ID)
					{
						cust1 = CheckCommandAccess(to, "somefuckingcommandlolwut", ADMFLAG_CUSTOM1, true);
						cust2 = CheckCommandAccess(to, "somefuckingcommandlolwut", ADMFLAG_CUSTOM2, true);
					}
					if (!IsPlayerAlive(to) || IsPlayerAlive(client) == IsPlayerAlive(to) || cust1 || cust2)
					{
						CPrintToChatEx(to, client, format_chat);
					}
				}
				else
				{
					CPrintToChatEx(to, client, format_chat);
				}
			}
		}
	}
	CRemoveTags(chatMessage, sizeof(chatMessage));
	LogToGame("\"%L\" %s \"%s\"", client, cmd, chatMessage); 
	return Plugin_Handled;
}

public Action:OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	if (!IsValidClient(attacker)) { return Plugin_Continue; }
	
	new victim = GetClientOfUserId(GetEventInt(event, "userid"));
	if (!IsValidClient(victim)) { return Plugin_Continue; }
	
	afkPoints[attacker] = 0;
	
	if(GetConVarInt(pg_statistics) == 0)
	{
		return Plugin_Continue;
	}
	else if(GetConVarInt(pg_statistics) == 2 && game_id == 2)
	{
		// [CS:GO] TTT
		// if attacker is not a traitor, return
		// if victimm is a traitor, return
		return Plugin_Continue;
	}
	else if(GetConVarInt(pg_statistics) == 3 && game_id == 2)
	{
		// [CS:GO] Jail Break
		if(GetClientTeam(attacker) != 2 && GetClientTeam(victim) != 3)
		{
			return Plugin_Continue;
		}
	}
	else if(GetConVarInt(pg_statistics) == 4 && game_id == 3)
	{
		// [TF2] Freak Fortress
		if(GetClientTeam(attacker) == 3 && GetClientTeam(victim) != 2)
		{
			return Plugin_Continue;
		}
	}
	
	if(victim == attacker) { return Plugin_Continue; }
	
	new String:community_id_victim[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(victim, AuthId_SteamID64, community_id_victim, MAX_COMMUNITYID_LENGTH);
	
	new String:community_id_attacker[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(attacker, AuthId_SteamID64, community_id_attacker, MAX_COMMUNITYID_LENGTH);
	
	SaveField(community_id_victim, "killstreak", 0);

	SaveField(community_id_attacker, "killstreak", GetField(community_id_attacker, "killstreak") + 1);
	SaveField(community_id_victim, "killstreak", 0);
	GetMsgKillStreak(attacker, GetField(community_id_attacker, "killstreak"));
	
	SaveField(community_id_victim, "deaths", GetField(community_id_victim, "deaths") + 1);
	
	SaveField(community_id_attacker, "kills", GetField(community_id_attacker, "kills") + 1);
	
	new String:weapon[64];
	GetEventString(event, "weapon", weapon, sizeof(weapon));
	
	new headshot = 0;
	new revenge = 0;
	new domination = 0;
	if (game_id == 3)
	{
		if (GetEventInt(event, "customkill") == 1) { headshot = 1; }
		new death_flags = GetEventInt(event,"death_flags");
		if(death_flags & 4) { revenge = 1; GetMsgRevenge(attacker,victim); }
		if(death_flags & 1) { domination = 1; GetMsgDomination(attacker,victim); }
	}
	else
	{
		if (GetEventBool(event, "headshot")) { headshot = 1; }
		if (GetEventInt(event, "revenge")) { revenge = 1; GetMsgRevenge(attacker,victim); }
		if (GetEventInt(event, "dominated")) { domination = 1; GetMsgDomination(attacker,victim); }
	}
	
	new String:query[6500];
	new assister = GetClientOfUserId(GetEventInt(event, "assister"));
	if(IsValidClient(assister) && victim != assister)
	{
		new String:community_id_assister[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(assister, AuthId_SteamID64, community_id_assister, MAX_COMMUNITYID_LENGTH);
		SaveField(community_id_assister, "assists", GetField(community_id_assister, "assists") + 1);
		
		Format(query, sizeof(query), QRY_STATS[3], server_ip, community_id_attacker, community_id_victim, community_id_assister, weapon, headshot, revenge, domination);
	}
	else
	{
		Format(query, sizeof(query), QRY_STATS[4], server_ip, community_id_attacker, community_id_victim, weapon, headshot, revenge, domination);
	}
	SQL_TQuery(hDatabase, QRY_NONE, query);
	
	GetMsgRank(attacker);
	GetMsgRank(assister);
	
	return Plugin_Continue;
}