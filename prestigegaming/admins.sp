// Work in progress (intended to replace: admin-sql-threaded.sp)
public StartAdmins()
{
	new String:query[512];
	new String:error[512];
	Format(query, sizeof(query), "CREATE TABLE IF NOT EXISTS `admins` (`user_id` INT(11), `ply_id` BIGINT(64), `ply_name` VARCHAR(65), `groups` VARCHAR(255), `time` BIGINT(64) )");
	if (!SQL_Query(hCache, query)) {
		SQL_GetError(hCache, error, sizeof(error));
		SetFailState(error);
		PrintToServer(error);
	}
	Format(query, sizeof(query), "DELETE FROM admins WHERE time < strftime('%%s') - 604800");
	if (!SQL_Query(hCache, query)) {
		SQL_GetError(hCache, error, sizeof(error));
		SetFailState(error);
		PrintToServer(error);
	}
}

public AdminQueryCallback(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) {
		PrintToServer("AdminQueryCallback Error - %s - %s", error, data);
	}
	new client = data;
	new bool:result_found = false; // Should be false
	// Using Sample Data for now
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	new String:ply_name[36];
	GetClientName(client, ply_name, sizeof(ply_name));
	
	new admin_user_id;
	new String:admin_name[40] = "";
	new String:admin_groups[40] = "";
	if (!IsValidClient(client)) {
		return false;
	}
	if (SQL_FetchRow(hndl)) {
		result_found = true;
		admin_user_id = SQL_FetchInt(hndl, 0);
		SQL_FetchString(hndl, 1, admin_name, sizeof(admin_name));
		SQL_FetchString(hndl, 2, admin_groups, sizeof(admin_groups));
	}
	/*if (result_found) { // Update the cache
		new String:query[512];
		new String:error2[512];
		Format(query, sizeof(query), "REPLACE INTO admins (user_id, ply_id, ply_name, groups) VALUES(%i, '%s', '%s', '%s')", admin_user_id, ply_id, admin_name, admin_groups);
		if (!SQL_Query(hCache, query)) {
			SQL_GetError(hCache, error2, sizeof(error2));
			SetFailState(error2);
		}
	} else { // Check the Cache
		new String:query[512];
		PrintToServer("No DB Result found for %N - Falling back to Cache", client);
		Format(query, sizeof(query), "SELECT user_id, ply_name, groups FROM admins WHERE ply_id = '%s'", ply_id);
		new Handle:hQuery = SQL_Query(hCache, query);
		if (SQL_FetchRow(hQuery)) {
			admin_user_id = SQL_FetchInt(hQuery, 0);
			SQL_FetchString(hQuery, 1, admin_name, sizeof(admin_name));
			SQL_FetchString(hQuery, 2, admin_groups, sizeof(admin_groups));
			result_found = true;
		}
	}*/
	if (result_found) {
		new String:groups[20][10];
		// explode them 
		ExplodeString(admin_groups, ",", groups, 20, 10);
		
		// since there are groups, lets create them as an admin if not already
		new AdminId:admin = GetUserAdmin(client);
		if(admin == INVALID_ADMIN_ID)
		{
			admin = CreateAdmin(admin_name);
			SetUserAdmin(client, admin); // This may need to happen after the for loop - Not sure
			user_id[client] = admin_user_id;
		}
		
		// loop through each groupid so we can give them access to the group if it exists
		for (new i = 0; i < sizeof(groups); i++)
		{
			new String:g_name[64];
			new String:g_tag[16];
			new id = StringToInt(groups[i]);
			if (id == 0) continue;
			new GroupId:group;
			if (!GetAdminGroupName(id, g_name, sizeof(g_name), g_tag, sizeof(g_tag))) {continue;}
			if (!StrEqual(g_tag, "")) {
				SetArrayString(hTags, client, g_tag);
			}
			// we need to loop through all the group id's that are exploded
			// then we need to make sure that the groupid(name) exists in the sourcemod admin_groups.cfg file by using the following IF statement
			group = FindAdmGroup(g_name);
			if (group != INVALID_GROUP_ID) {
				// give them the group through this function.
				AdminInheritGroup(admin, group);
			} else {
				PrintToServer("Group NOT Found %s", g_name);
			}
			
		}
	}
	NotifyPostAdminCheck(client); // This function MUST be called - even if we don't give the client any admin
	return false;
}

public GetAdminGroupName(gid, String:name[], name_size, String:tag[], tag_size) {
	if (gid == 3 || gid == 5 || gid == 30 || gid == 4) { 
		// All Leadership on the same group because the permissions are all the same
		strcopy(name, name_size, "Leadership");
		strcopy(tag, tag_size, "=[pG]=");
	}
	else if (gid == 10) {
		strcopy(name, name_size, "Member");
		strcopy(tag, tag_size, "=[pG]=");
	}
	else if (gid == 63) {
		strcopy(name, name_size, "Junior");
		strcopy(tag, tag_size, "[pG]");
	}
	else if (gid == 11) { // Recruit
		strcopy(name, name_size, "Recruit");
		strcopy(tag, tag_size, "-pg-");
	}
	else if (gid == 25) {
		strcopy(name, name_size, "[AT]");
	}
	else if (gid == 51){
		strcopy(name, name_size, "[EC]");
	}
	else if (gid == 62){
		strcopy(name, name_size, "[TT]");
	}
	else if (gid == 13){
		strcopy(name, name_size, "Dedicated Supporter");
	}
	else if (gid == 77){
		strcopy(name, name_size, "[vip] CS");
	}
	else if (gid == 76){
		strcopy(name, name_size, "[vip] TF");
	} else if (gid == 52) {
		strcopy(name, name_size, "[admin] Global");
	} else if (gid == 32) {
		strcopy(name, name_size, "[admin] TTT");
	} else if (gid == 85) {
		strcopy(name, name_size, "[admin] JB");
	} else if (gid == 45) {
		strcopy(name, name_size, "[admin] Training");
	} else {
		return false;
	}
	return true;
}