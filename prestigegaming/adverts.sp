public StartAdverts()
{
	CreateTimer(300.0, GetAdvertMsg, _, TIMER_REPEAT);
}

public Action:GetAdvertMsg(Handle:timer)
{
	if (advertNo == 0)
	{
		CPrintToChatAll("{green}--------------------");
		CPrintToChatAll("{lightgreen}We are recruiting mature players!");
		CPrintToChatAll("{lightgreen}Apply at {green}www.prestige-gaming.org");
		CPrintToChatAll("{green}--------------------");
		advertNo += 1;
	}
	else if(advertNo == 1)
	{
		CPrintToChatAll("{green}--------------------");
		CPrintToChatAll("{lightgreen}Like this server? Add it to your favorites!");
		CPrintToChatAll("{green}%s:27015",server_ip);
		CPrintToChatAll("{green}--------------------");
		advertNo += 1;
	}
	else if(advertNo == 2)
	{
		CPrintToChatAll("{green}--------------------");
		CPrintToChatAll("{lightgreen}We are always looking for admins!");
		CPrintToChatAll("{lightgreen}Apply at {green}www.prestige-gaming.org");
		CPrintToChatAll("{green}--------------------");
		advertNo += 1;
	}
	else
	{
		CPrintToChatAll("{green}--------------------");
		CPrintToChatAll("{lightgreen}Please donate to support our servers.");
		CPrintToChatAll("{lightgreen}Perks: {green}!votekick{lightgreen}, {green}!voteban {lightgreen}& more.");
		CPrintToChatAll("{green}--------------------");
		advertNo = 0;
	}
}