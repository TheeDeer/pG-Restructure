public StartAFK()
{
	CreateTimer(10.0, AFKTick, _, TIMER_REPEAT);
}

public Action:AFKTick(Handle:timer)
{
	for (new client = 1; client <= MAXPLAYERS - 1; client++)
	{
		if (!IsValidClient(client))
		{
			return;
		}
		
		if(GetClientTeam(client) == 1)
		{
			if (afkPoints[client] > 15)
			{
				timeAway[client] += 10;
			}
			else
			{
				afkPoints[client] += 1;
			}
		}
		else
		{
			afkPoints[client] = 0;
		}
	}
}