public StartChat()
{
	LoadTranslations("common.phrases");
	
	RegAdminCmd("sm_say", Command_SmSay, ADMFLAG_CHAT, "sm_say <message> - sends message to all players");
	RegAdminCmd("sm_csay", Command_SmCsay, ADMFLAG_CHAT, "sm_csay <message> - sends centered message to all players");
	RegConsoleCmd("sm_chat", Command_SmChat, "sm_chat <message> - sends message to admins");
	RegAdminCmd("sm_psay", Command_SmPsay, ADMFLAG_CHAT, "sm_psay <name or #userid> <message> - sends private message");
	RegAdminCmd("sm_msay", Command_SmMsay, ADMFLAG_CHAT, "sm_msay <message> - sends message as a menu panel");
	
	sv_deadtalk 		= CreateConVar("sv_deadtalk", "0", "Dead players can speak (voice, text) to the living");
	sv_deadtalk_override= CreateConVar("sv_deadtalk_override", "0", "ignores sv_deadtalk and hides text chat");
	
	sv_teamchat 		= CreateConVar("sm_teamchat", "1", "Sets if teamchat is enabled");
	sv_teamchat_error 	= CreateConVar("sm_teamchat_error", "1", "If sm_teamchat=0, Should a error be displayed");

	g_formatChat 		= CreateGlobalForward("FormatChat", ET_Ignore, Param_Cell, Param_String);
	g_formatName 		= CreateGlobalForward("FormatName", ET_Ignore, Param_Cell, Param_String);
}

public Action:Command_SmSay(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_say <message>");
		return Plugin_Handled;	
	}
	
	decl String:text[192];
	GetCmdArgString(text, sizeof(text));

	SendChatToAll(client, text);
	
	return Plugin_Handled;		
}

public Action:Command_SmCsay(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_csay <message>");
		return Plugin_Handled;	
	}
	
	decl String:text[192];
	GetCmdArgString(text, sizeof(text));
	
	DisplayCenterTextToAll(client, text);
	
	return Plugin_Handled;		
}

public Action:Command_SmHsay(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_hsay <message>");
		return Plugin_Handled;  
	}
	
	decl String:text[192];
	GetCmdArgString(text, sizeof(text));
 
	decl String:nameBuf[MAX_NAME_LENGTH];
	
	for (new i = 1; i <= MaxClients; i++)
	{
		if (!IsClientInGame(i) || IsFakeClient(i))
		{
			continue;
		}
		FormatActivitySource(client, i, nameBuf, sizeof(nameBuf));
		PrintHintText(i, "%s: %s", nameBuf, text);
	}
	
	return Plugin_Handled;	
}

public Action:Command_SmChat(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_chat <message>");
		return Plugin_Handled;	
	}
	
	new lastMsg = GetTime();
	if(antiChatSpam[client] != 0 && (lastMsg - antiChatSpam[client]) < 1.2)
	{
		antiChatSpam[client] = lastMsg;
		PrintToChat(client, "[SM] You are flooding the chat!");
		return Plugin_Stop;
	}
	antiChatSpam[client] = lastMsg;
	
	decl String:text[192];
	GetCmdArgString(text, sizeof(text));

	SendChatToAdmins(client, text);
	
	return Plugin_Handled;	
}

public Action:Command_SmPsay(client, args)
{
	if (args < 2)
	{
		ReplyToCommand(client, "[SM] Usage: sm_psay <name or #userid> <message>");
		return Plugin_Handled;	
	}	
	
	decl String:text[192], String:arg[64];
	GetCmdArgString(text, sizeof(text));

	new len = BreakString(text, arg, sizeof(arg));
	
	new target = FindTarget(client, arg, false, false);
		
	if (target == -1)
		return Plugin_Handled;	
	
	SendPrivateChat(client, target, text[len]);
	
	return Plugin_Handled;	
}

public Action:Command_SmMsay(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: sm_msay <message>");
		return Plugin_Handled;	
	}
	
	decl String:text[192];
	GetCmdArgString(text, sizeof(text));

	SendPanelToAll(client, text);
	
	return Plugin_Handled;		
}

SendChatToAll(client, const String:message[])
{
	new String:nameBuf[MAX_NAME_LENGTH];
	
	for (new i = 1; i <= MaxClients; i++)
	{
		if (!IsClientInGame(i) || IsFakeClient(i))
		{
			continue;
		}
		FormatActivitySource(client, i, nameBuf, sizeof(nameBuf));
		
		CPrintToChat(i, "{green}(ALL) %s: %s", nameBuf, message);
	}
}

DisplayCenterTextToAll(client, const String:message[])
{
	new String:nameBuf[MAX_NAME_LENGTH];
	
	for (new i = 1; i <= MaxClients; i++)
	{
		if (!IsClientInGame(i) || IsFakeClient(i))
		{
			continue;
		}
		FormatActivitySource(client, i, nameBuf, sizeof(nameBuf));
		PrintCenterText(i, "%s: %s", nameBuf, message);
	}
}

SendChatToAdmins(from, const String:message[])
{
	new fromAdmin = CheckCommandAccess(from, "sm_say", ADMFLAG_CHAT);
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && (from == i || CheckCommandAccess(i, "sm_say", ADMFLAG_CHAT)))
		{
			CPrintToChat(i, "{red}(%sADMINS) %N: %s", fromAdmin ? "" : "TO ", from, message);
		}	
	}
}

SendPrivateChat(client, target, const String:message[])
{
	if (!client)
	{
		PrintToServer("{red}(Private to %N) %N: %s", target, client, message);
	}
	else if (target != client)
	{
		CPrintToChat(target, "{red}(Private to %N) %N: %s", target, client, message);
		for (new i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i) && CheckCommandAccess(i, "sm_say", ADMFLAG_CHAT))
			{
				if (target != i)
				{
					CPrintToChat(i, "{red}(Private to %N) %N: %s", target, client, message);
				}
			}	
		}
	}
}

void SendPanelToAll(int from, char[] message)
{
	char title[100];
	Format(title, 64, "%N:", from);
	
	ReplaceString(message, 192, "\\n", "\n");
	
	Panel mSayPanel = CreatePanel();
	mSayPanel.SetTitle(title);
	DrawPanelItem(mSayPanel, "", ITEMDRAW_SPACER);
	DrawPanelText(mSayPanel, message);
	DrawPanelItem(mSayPanel, "", ITEMDRAW_SPACER);

	SetPanelCurrentKey(mSayPanel, 10);
	DrawPanelItem(mSayPanel, "Exit", ITEMDRAW_CONTROL);

	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && !IsFakeClient(i))
		{
			SendPanelToClient(mSayPanel, i, Handler_DoNothing, 10);
		}
	}

	delete mSayPanel;
}

public Handler_DoNothing(Menu menu, MenuAction action, int param1, int param2)
{
	/* Do nothing */
}