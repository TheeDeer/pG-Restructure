ConnectDatabase()
{
	SQL_TConnect(GetDatabase);
}

public GetDatabase(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	//if (hndl == INVALID_HANDLE)
	//{
		// We need to connect to second database here
		//SetFailState(error);
	//}
	// I have changed things to use a cache. This makes the second database largely unnecessary. ~Jon
	hDatabase = hndl;
	
	// The following is needed as it is run too early if inside statistics.sp
	if(GetConVarInt(pg_statistics) != 0)
	{
		GetAllClientStats();
		GetTop();
	}
}

public QRY_NONE(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
	{
		return;
	}
}