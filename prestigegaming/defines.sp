/* This file will hold all the variables for the pG plugin for ease of access */


/*============================================
=           	 Definitions 		         =
=============================================*/

/*----------  prestigegaming.sp  ----------*/
#pragma semicolon 1

#define PLUGIN_NAME "Prestige Gaming"
#define PLUGIN_AUTHOR "Abdul & TheeDeer"
#define PLUGIN_DESC "pG Core - Visit us at www.prestige-gaming.org""
#define PLUGIN_VERSION "3.0"
#define PLUGIN_URL "http://www.prestige-gaming.org"

#define IsClient(%1) ( 0 < %1 < MaxClients )
#define MAX_COMMUNITYID_LENGTH 18
#define MAX_STEAMID32_LENGTH 21


/*----------  serverstatus.sp  ----------*/
#define SERVER_INITIAL_WAIT 30.0
#define SERVER_GENERAL_WAIT 300.0


/*----------  chat.sp  ----------*/
#define CHAT_SYMBOL '@'
#define MAX_MESSAGE 200


/*----------  tips.sp  ----------*/
#define FILE_PATH "addons/sourcemod/configs/prestigegaming_tips.cfg"

/*===========================================
=                  Variables                =
============================================*/

/*----------  Server Information  ----------*/
new String:server_ip[16];
new server_id = -1;

new String:mapName[255];
new map_id = -1;
new mapTime;

/*----------  prestigegaming.sp  ----------*/
new bool:in_name_change[MAXPLAYERS + 1];
new bool:isBanned[MAXPLAYERS + 1] = { false, ... };

new mapTime;
new game_id = 0;
new sessionId[MAXPLAYERS + 1];


/*----------  chat.sp  ----------*/
new antiChatSpam[MAXPLAYERS + 1];


/*----------  admins.sp  ----------*/
new user_id[MAXPLAYERS+1];


/*----------  adverts.sp  ----------*/
new advertNo = 0;


/*----------  afk.sp  ----------*/
new afkPoints[MAXPLAYERS + 1];
new timeAway[MAXPLAYERS + 1];


/*----------  statistics.sp  ----------*/
new String:ply_top[51][MAX_COMMUNITYID_LENGTH];



/*===========================================
=                   Handles                 =
============================================*/

/*----------  prestigegaming.sp  ----------*/
new Handle:hDatabase = INVALID_HANDLE;
new Handle:hCache = INVALID_HANDLE;
new Handle:hTags = INVALID_HANDLE;


/*----------  chat.sp  ----------*/
new Handle:sv_deadtalk;
new Handle:sv_deadtalk_override;
new Handle:sv_teamchat;
new Handle:sv_teamchat_error;
new Handle:g_formatChat;
new Handle:g_formatName;


/*----------  statistics.sp  ----------*/
new Handle:pg_statistics;
new Handle:ply_players = INVALID_HANDLE;


/*----------  tips.sp  ----------*/
new Handle: g_hMessages;

/*----------  serverstatus.sp  ----------*/
new Handle:UpdateStatus = INVALID_HANDLE;

/*===========================================
=              Predefined arrays            =
============================================*/

/*----------  prestigegaming.sp  ----------*/
new const String:QRY_BAN[4][] =
{
	"SELECT * FROM (SELECT ban_id, target_id, target_ip, ban_length, ban_reason, round((timestamp+(60*ban_length)-UNIX_TIMESTAMP(now()))/60) AS ban_remaining FROM `pg_bans` WHERE ban_status = '0' AND ban_type = '0' AND (`target_id` = '%s' OR `target_ip` = '%s')) AS bans WHERE ban_length = '0' OR ban_remaining > '0' ORDER BY ban_remaining DESC",
	"INSERT INTO `pg_bans` (`server_ip`, `target_id`, `target_name`, `ban_length`, `ban_reason`, `timestamp`) VALUES ('%s','%s','%s','%i','Ban Evasion (%s)',unix_timestamp(now()))",
	"INSERT INTO `pg_bans` (`server_ip`, `target_id`, `target_ip`, `target_name`, `ban_length`, `ban_reason`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', '%i', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_bans` (`server_ip`, `ply_id`, `ply_user_id`, `ply_ip`, `ply_name`, `target_id`, `target_ip`, `target_name`, `ban_length`, `ban_reason`, `timestamp`) VALUES ('%s', '%s', '%i', '%s', '%s', '%s', '%s', '%s', '%i', '%s', unix_timestamp(now()))"
};


/*----------  chat.sp  ----------*/
new const String:filterWords[17][] = 
{
	"niger",
	"nigger",
	"nugger",
	"n1gger",
	"nlgger",
	"nigga",
	"negro",
	"negga",
	"asshole",
	"cunt",
	"cuck",
	"prick",
	"faggot",
	"faggit",
	"queer",
	"moron",
	"squeaker"
};


/*----------  History.sp  ----------*/
new const String:QRY_HISTORY[4][] = {
	"SELECT server_ip, ply_id, ply_name, cmd, target_id, target_name, args, FROM_UNIXTIME(TIMESTAMP), TIMESTAMP FROM pg_logs WHERE ply_id = %s OR target_id = %s UNION SELECT server_ip, ply_id, ply_name,  'sm_ban' AS cmd, target_id, target_name, CONCAT( ban_length,  ' ', ban_reason ) AS args, FROM_UNIXTIME(TIMESTAMP), TIMESTAMP FROM pg_bans WHERE ply_id = %s OR target_id = %s ORDER BY TIMESTAMP DESC LIMIT %i, 50",
	"SELECT (SELECT COUNT(log_id) FROM pg_logs WHERE ply_id = '%s' OR target_id = '%s') as c1, (SELECT COUNT(ban_id) FROM pg_bans WHERE ply_id = '%s' OR target_id = '%s') as c2",
	"SELECT server_ip, ply_id, ply_name, cmd, target_id, target_name, args, FROM_UNIXTIME(TIMESTAMP), TIMESTAMP FROM pg_logs WHERE (ply_id = %s OR target_id = %s) AND cmd NOT IN('say', 'say_team', 'sm_chat', 'sm_say', 'connect', 'disconnect') UNION SELECT server_ip, ply_id, ply_name,  'sm_ban' AS cmd, target_id, target_name, CONCAT( ban_length,  ' ', ban_reason ) AS args, FROM_UNIXTIME(TIMESTAMP), TIMESTAMP FROM pg_bans WHERE ply_id = %s OR target_id = %s ORDER BY TIMESTAMP DESC LIMIT %i, 50",
	"SELECT (SELECT COUNT(log_id) FROM pg_logs WHERE (ply_id = '%s' OR target_id = '%s') AND cmd NOT IN('say', 'say_team', 'sm_chat', 'sm_say', 'connect', 'disconnect') ) as c1, (SELECT COUNT(ban_id) FROM pg_bans WHERE ply_id = '%s' OR target_id = '%s') as c2"
};


/*----------  Logging.sp  ----------*/
new const String:QRY_LOG[12][] =
{
	"INSERT INTO `pg_logs` (`server_ip`, `ply_id`, `ply_name`, `cmd`, `args`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `cmd`, `args`, `timestamp`) VALUES ('%s', '%s', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `ply_id`, `ply_name`, `cmd`, `target_id`, `target_name`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `cmd`, `target_id`, `target_name`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `ply_id`, `ply_name`, `cmd`, `target_id`, `target_name`, `args`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `cmd`, `target_id`, `target_name`, `args`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', '%s', unix_timestamp(now()))",
	"",
	"INSERT INTO `pg_logs` (`server_ip`, `ply_id`, `ply_name`, `cmd`, `args`, `timestamp`) VALUES ('%s', '%s', '%s', 'disconnect', '%i;%i;%i', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `ply_id`, `ply_name`, `cmd`, `args`, `timestamp`) VALUES ('%s', '%s', '%s', 'connect', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `cmd`, `args`, `timestamp`) VALUES ('%s', 'changelevel', '%s', '%i')",
	"INSERT INTO `pg_logs` (`server_ip`, `ply_id`, `ply_name`, `cmd`, `timestamp`) VALUES ('%s', '%s', '%s', '%s', unix_timestamp(now()))",
	"INSERT INTO `pg_logs` (`server_ip`, `cmd`, `timestamp`) VALUES ('%s', '%s', unix_timestamp(now()))"
};


/*----------  reviews.sp  ----------*/
new const String:QRY_REP[2][] =
{
	"SELECT `id` FROM `pg_reviews` WHERE UNIX_TIMESTAMP(now()) <= (timestamp+86400) AND `ply_user_id` = '%i' AND `target_user_id` = '%i'",
	"INSERT INTO `pg_reviews` (`ply_user_id`, `target_user_id`, `type`, `note`, `timestamp`) VALUES ('%i', '%i', '%i', '%s', unix_timestamp(now()))"
};


/*----------  statistics.sp  ----------*/
new const String:QRY_STATS[5][] =
{
	"CALL pg_stats_top('%s')",
	"CALL pg_stats_client('%s','%s')",
	"CALL pg_stats_next('%i','%s')",
	"INSERT INTO `pg_stats` (`server_ip`, `attacker_id`, `victim_id`, `assist_id`, `weapon`, `headshot`, `revenge`, `domination`, `timestamp`) VALUES ('%s','%s','%s','%s','%s','%i','%i','%i',unix_timestamp(now()))",
	"INSERT INTO `pg_stats` (`server_ip`, `attacker_id`, `victim_id`, `weapon`, `headshot`, `revenge`, `domination`, `timestamp`) VALUES ('%s','%s','%s','%s','%i','%i','%i',unix_timestamp(now()))"
};