public StartHistory()
{
	RegAdminCmd("sm_history", Command_History, ADMFLAG_KICK, "sm_history <player>");
	RegAdminCmd("sm_actions", Command_Actions, ADMFLAG_KICK, "sm_actions <player>");
}

public Action:Command_History (client, args)
{
	if (args < 1) { ReplyToCommand(client, "[SM] This command requires a target"); return Plugin_Handled;}

	new target = client, page = 0;
	new String:arg1[MAX_TARGET_LENGTH], String:arg2[64];
	GetCmdArg(1, arg1, MAX_TARGET_LENGTH);
	target = FindTarget(client, arg1, true, true);
	if (args >= 2) { GetCmdArg(2, arg2, sizeof(arg2)); }
	page = StringToInt(arg2) - 1;
	if (page < 0) page = 0;

	if (!IsValidClient(target)) { ReplyToCommand(client, "[SM] No matching client"); return Plugin_Handled;}
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

	new String:ply_name[256];
	GetClientName(target, ply_name, sizeof(ply_name));

	new Handle:dataPack = CreateDataPack();
	WritePackCell(dataPack, client);
	WritePackString(dataPack, ply_id);
	WritePackString(dataPack, ply_name);
	WritePackCell(dataPack, page);
	ResetPack(dataPack);

	new String:query[600];
	Format(query, sizeof(query), QRY_HISTORY[1], ply_id, ply_id, ply_id, ply_id);
	SQL_TQuery(hDatabase, Query_History_Pages, query, dataPack);

	return Plugin_Handled;
}

public Query_History_Pages(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) { SetFailState(error); return;}
	new client, String:ply_id[MAX_COMMUNITYID_LENGTH], String:ply_name[256], page;
	client = ReadPackCell(data);
	ReadPackString(data, ply_id, sizeof(ply_id));
	ReadPackString(data, ply_name, sizeof(ply_name));
	page = ReadPackCell(data);

	if (SQL_FetchRow(hndl))
	{
		new results = SQL_FetchInt(hndl, 0) + SQL_FetchInt(hndl, 1);
		new pages = results / 50;
		if (results % 50 > 0)
			pages++;
		WritePackCell(data, pages);
	}
	else
	{
		WritePackCell(data, 0);
	}

	ResetPack(data);


	new String:query[600];
	Format(query, sizeof(query), QRY_HISTORY[0], ply_id, ply_id, ply_id, ply_id, page * 50);
	SQL_TQuery(hDatabase, Query_History, query, data);
}

public Action:Command_Actions (client, args)
{
	if (args < 1) { ReplyToCommand(client, "[SM] This command requires a target"); return Plugin_Handled;}

	new target = client, page = 0;
	new String:arg1[MAX_TARGET_LENGTH], String:arg2[64];
	GetCmdArg(1, arg1, MAX_TARGET_LENGTH);
	target = FindTarget(client, arg1, true, true);
	if (args >= 2) { GetCmdArg(2, arg2, sizeof(arg2)); }
	page = StringToInt(arg2) - 1;
	if (page < 0) page = 0;

	if (!IsValidClient(target)) { ReplyToCommand(client, "[SM] No matching client"); return Plugin_Handled;}
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

	new String:ply_name[256];
	GetClientName(target, ply_name, sizeof(ply_name));

	new Handle:dataPack = CreateDataPack();
	WritePackCell(dataPack, client);
	WritePackString(dataPack, ply_id);
	WritePackString(dataPack, ply_name);
	WritePackCell(dataPack, page);
	ResetPack(dataPack);

	new String:query[600];
	Format(query, sizeof(query), QRY_HISTORY[3], ply_id, ply_id, ply_id, ply_id);
	SQL_TQuery(hDatabase, Query_Actions_Pages, query, dataPack);

	return Plugin_Handled;
}

public Query_Actions_Pages(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) { SetFailState(error); return;}
	new client, String:ply_id[MAX_COMMUNITYID_LENGTH], String:ply_name[256], page;
	client = ReadPackCell(data);
	ReadPackString(data, ply_id, sizeof(ply_id));
	ReadPackString(data, ply_name, sizeof(ply_name));
	page = ReadPackCell(data);

	if (SQL_FetchRow(hndl))
	{
		new results = SQL_FetchInt(hndl, 0) + SQL_FetchInt(hndl, 1);
		new pages = results / 50;
		if (results % 50 > 0)
			pages++;
		WritePackCell(data, pages);
	}
	else
	{
		WritePackCell(data, 0);
	}

	ResetPack(data);


	new String:query[600];
	Format(query, sizeof(query), QRY_HISTORY[2], ply_id, ply_id, ply_id, ply_id, page * 50);
	SQL_TQuery(hDatabase, Query_History, query, data);
}

public Query_History(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) { SetFailState(error); return;}
	new client, String:ply_id[MAX_COMMUNITYID_LENGTH], String:ply_name[256], page, pages;
	client = ReadPackCell(data);
	ReadPackString(data, ply_id, sizeof(ply_id));
	ReadPackString(data, ply_name, sizeof(ply_name));
	page = ReadPackCell(data);
	pages = ReadPackCell(data);

	if (!IsValidClient(client)) return;
	PrintToConsole(client, "[SM] History for %s [%s] Page # %i/%i", ply_name, ply_id, page + 1, pages);

	while (SQL_FetchRow(hndl))
	{
		new String:ply_id2[MAX_COMMUNITYID_LENGTH];
		SQL_FetchString(hndl, 1, ply_id2, sizeof(ply_id2));
		new String:ply_name2[128];
		SQL_FetchString(hndl, 2, ply_name2, sizeof(ply_name2));
		new String:cmd[128];
		SQL_FetchString(hndl, 3, cmd, sizeof(cmd));
		new String:target_id[MAX_COMMUNITYID_LENGTH];
		SQL_FetchString(hndl, 4, target_id, sizeof(target_id));
		new String:target_name[128];
		SQL_FetchString(hndl, 5, target_name, sizeof(target_name));
		new String:args[256];
		SQL_FetchString(hndl, 6, args, sizeof(args));
		new String:timestamp[64];
		SQL_FetchString(hndl, 7, timestamp, sizeof(timestamp));
		PrintHistoryLine(client, ply_name2, ply_id2, cmd, target_name, target_id, args, timestamp);
	}

}

PrintHistoryLine(client, String:ply[], String:ply_id[], String:cmd[], String:target[], String:target_id[], String:args[], String:timestamp[])
{
	if (StrEqual(cmd, "sm_mute"))
	{
		PrintToConsole(client, "<%s> %s [%s] was muted by %s [%s]", timestamp, target, target_id, ply, ply_id);
	}
	else if (StrEqual(cmd, "sm_unmute"))
	{
		PrintToConsole(client, "<%s> %s [%s] was unmuted by %s [%s]", timestamp, target, target_id, ply, ply_id);
	}
	else if (StrEqual(cmd, "sm_gag"))
	{
		PrintToConsole(client, "<%s> %s [%s] was gagged by %s [%s]", timestamp, target, target_id, ply, ply_id);
	}
	else if (StrEqual(cmd, "sm_ungag"))
	{
		PrintToConsole(client, "<%s> %s [%s] was ungagged by %s [%s]", timestamp, target, target_id, ply, ply_id);
	}
	else if (StrEqual(cmd, "sm_silence"))
	{
		PrintToConsole(client, "<%s> %s [%s] was silenced by %s [%s]", timestamp, target, target_id, ply, ply_id);
	}
	else if (StrEqual(cmd, "sm_unsilence"))
	{
		PrintToConsole(client, "<%s> %s [%s] was unsilenced by %s [%s]", timestamp, target, target_id, ply, ply_id);
	}
	else if (StrEqual(cmd, "say"))
	{
		PrintToConsole(client, "<%s> %s: %s", timestamp, ply, args);
	}
	else if (StrEqual(cmd, "say_team"))
	{
		PrintToConsole(client, "<%s> (TEAM) %s: %s", timestamp, ply, args);
	}
	else if (StrEqual(cmd, "sm_chat"))
	{
		PrintToConsole(client, "<%s> (ADMINS) %s: %s", timestamp, ply, args);
	}
	else if (StrEqual(cmd, "sm_say"))
	{
		PrintToConsole(client, "<%s> (ALL) %s: %s", timestamp, ply, args);
	}
	else if (StrEqual(cmd, "sm_psay"))
	{
		PrintToConsole(client, "(Private to <%s>) %s [%s]: %s", timestamp, ply, target, target_id, args);
	}
	else if (StrEqual(cmd, "sm_kick"))
	{
		PrintToConsole(client, "<%s> %s [%s] was kicked by %s [%s] for %s", timestamp, target, target_id, ply, ply_id, args);
	}
	else if (StrEqual(cmd, "sm_votekick"))
	{
		PrintToConsole(client, "<%s> %s [%s] is being vote-kicked by %s [%s] for %s", timestamp, target, target_id, ply, ply_id, args);
	}
	else if (StrEqual(cmd, "sm_voteban"))
	{
		PrintToConsole(client, "<%s> %s [%s] is being vote-banned by %s [%s] for %s", timestamp, target, target_id, ply, ply_id, args);
	}
	else if (StrEqual(cmd, "sm_ban"))
	{
		new String:duration[32], reason_start;
		reason_start = SplitString(args, ";", duration, sizeof(duration));
		PrintToConsole(client, "<%s> %s [%s] was banned by %s [%s] for %s %s", timestamp, target, target_id, ply, ply_id, duration, args[reason_start]);
	}
	else if (StrEqual(cmd, "connect"))
	{
		PrintToConsole(client, "<%s> %s connected", timestamp, ply, ply_id);
	}
	else if (StrEqual(cmd, "disconnect"))
	{
		PrintToConsole(client, "<%s> %s disconnected", timestamp, ply, ply_id);
	}
	else
	{
		PrintToConsole(client, "No History Format found for %s", cmd);
	}
}
