public StartLogging()
{
	AddCommandListener(Command_One, "sm_say");
	AddCommandListener(Command_One, "sm_csay");
	AddCommandListener(Command_One, "sm_chat");
	AddCommandListener(Command_One, "sm_msay");
	
	AddCommandListener(Command_Two, "sm_mute");
	AddCommandListener(Command_Two, "sm_unmute");
	AddCommandListener(Command_Two, "sm_gag");
	AddCommandListener(Command_Two, "sm_ungag");
	AddCommandListener(Command_Two, "sm_silence");
	AddCommandListener(Command_Two, "sm_unsilence");
	
	AddCommandListener(Command_Three, "sm_voteban");
	AddCommandListener(Command_Three, "sm_votekick");
	AddCommandListener(Command_Three, "sm_kick");
	AddCommandListener(Command_Three, "sm_psay");
}

public Action:Command_None(client, String:cmd[], argc)
{
	new String:query[600];
	
	if (IsValidClient(client))
	{
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);
		
		Format(query, sizeof(query), QRY_LOG[10], server_ip, ply_id, ply_name, cmd);
	}
	else
	{
		Format(query, sizeof(query), QRY_LOG[11], server_ip, cmd);
	}
	
	SQL_TQuery(hDatabase, QRY_NONE, query, 0);
	
	return Plugin_Handled;
}

public Action:Command_One(client, String:cmd[], argc)
{
	if (argc < 1) { return Plugin_Handled; }
	
	new String:args[255];
	GetCmdArgString(args, 255);
	SQL_EscapeString(hDatabase, args, args, 255);
	
	new String:query[600];
	
	if (IsValidClient(client))
	{
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);
		
		Format(query, sizeof(query), QRY_LOG[0], server_ip, ply_id, ply_name, cmd, args);
	}
	else
	{
		Format(query, sizeof(query), QRY_LOG[1], server_ip, cmd, args);
	}
	
	SQL_TQuery(hDatabase, QRY_NONE, query, 0);
	
	return Plugin_Handled;
}

public Action:Command_Two(client, String:cmd[], argc)
{
	if (argc < 1) { return Plugin_Handled; }
	
	new target = client, String:arg1[MAX_TARGET_LENGTH];
	GetCmdArg(1, arg1, MAX_TARGET_LENGTH);
	target = FindTarget(client, arg1, true, true);
	
	if (!IsValidClient(target)) { return Plugin_Handled; }
	
	new String:target_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, target_id, MAX_COMMUNITYID_LENGTH);
	
	new String:target_name[255];
	GetClientName(target, target_name, sizeof(target_name));
	SQL_EscapeString(hDatabase, target_name, target_name, 255);
	
	new String:query[600];
	
	if (IsValidClient(client))
	{
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);
		
		Format(query, sizeof(query), QRY_LOG[2], server_ip, ply_id, ply_name, cmd, target_id, target_name);
	}
	else
	{
		Format(query, sizeof(query), QRY_LOG[3], server_ip, cmd, target_id, target_name);
	}
	
	SQL_TQuery(hDatabase, QRY_NONE, query, 0);
	
	return Plugin_Handled;
}

public Action:Command_Three(client, String:cmd[], argc)
{
	if (argc < 2) { return Plugin_Handled; }
	
	new target = client, String:arg1[MAX_TARGET_LENGTH];
	GetCmdArg(1, arg1, MAX_TARGET_LENGTH);
	target = FindTarget(client, arg1, true, true);
	
	if (!IsValidClient(target)) { return Plugin_Handled; }
	
	new String:target_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, target_id, MAX_COMMUNITYID_LENGTH);
	
	new String:target_name[255];
	GetClientName(target, target_name, sizeof(target_name));
	SQL_EscapeString(hDatabase, target_name, target_name, 255);
	
	decl String:args[192], String:arg[64];
	GetCmdArgString(args, sizeof(args));

	new len = BreakString(args, arg, sizeof(arg));
	Format(arg, sizeof(arg), args[len]);
	
	if (len == -1 || len == 0) { return Plugin_Handled; }
	
	SQL_EscapeString(hDatabase, arg, arg, 255);
	
	new String:query[600];
	
	if (IsValidClient(client))
	{
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);
		
		Format(query, sizeof(query), QRY_LOG[4], server_ip, ply_id, ply_name, cmd, target_id, target_name, arg);
	}
	else
	{
		Format(query, sizeof(query), QRY_LOG[4], server_ip, cmd, target_id, target_name, arg);
	}
	
	SQL_TQuery(hDatabase, QRY_NONE, query, 0);
	
	return Plugin_Handled;
}

public Action:SaveMapLog(Handle:timer)
{
	new String:currentMap[255];
	GetCurrentMap(currentMap, 255);
	if(StrEqual(mapName, currentMap, false))
	{
		// maybe we can just set the string to CurMap variable instead of getting it again
		GetCurrentMap(mapName, 255);
		
		new String:query[800];
		Format(query, sizeof(query), QRY_LOG[9], server_ip, mapName, mapTime);
		SQL_TQuery(hDatabase, QRY_NONE, query);
	}
}