/*
"SELECT `id` FROM `pg_reviews` WHERE UNIX_TIMESTAMP(now()) >= (timestamp+86400) AND `ply_id` = '%s' AND `target_id` = '%s'",
"INSERT INTO `pg_reviews` (`ply_id`, `target_id`, `type`, `note`, `timestamp`) VALUES ('%s', '%s', %i, '%s', unix_timestamp(now()))"
*/

public StartReviews()
{
	RegConsoleCmd("sm_up", Command_Review);
	RegConsoleCmd("sm_down", Command_Review);
}

public Action:Command_Review(client, args)
{
	if (!IsValidClient(client)) { return Plugin_Handled; }

	new AdminId:id = GetUserAdmin(client);
	if (id == INVALID_ADMIN_ID)
	{
		ReplyToCommand(client, "[SM] You do not have access to this command.");
		return Plugin_Handled;
	}
	
	if (!CheckCommandAccess(client, "member", ADMFLAG_CUSTOM6, true) && !CheckCommandAccess(client, "junior", ADMFLAG_CUSTOM5, true) && !CheckCommandAccess(client, "recruit", ADMFLAG_CUSTOM4, true))
	{
		ReplyToCommand(client, "[SM] You do not have access to this command.");
		return Plugin_Handled;
	}
	
	new String:arg[128];
	new String:message[256];

	GetCmdArg(1, arg, sizeof(arg));
	GetCmdArgString(message, sizeof(message));
	TrimString(message);
	
	if (strlen(message) <= 25)
	{
		ReplyToCommand(client, "[SM] Please enter a review longer than 25 characters.");
		return Plugin_Handled;
	}
	
	new target;
	target = FindTarget(client, arg, true, false);
	if (!IsValidClient(target)) { return Plugin_Handled; }

	if (client == target)
	{
		ReplyToCommand(client, "[SM] You cannot target yourself.");
		return Plugin_Handled;
	}

	id = GetUserAdmin(target);
	if (id == INVALID_ADMIN_ID)
	{
		ReplyToCommand(client, "[SM] You cannot target this player.");
		return Plugin_Handled;
	}
	
	if (!CheckCommandAccess(target, "junior", ADMFLAG_CUSTOM5, true) && !CheckCommandAccess(target, "recruit", ADMFLAG_CUSTOM4, true))
	{
		ReplyToCommand(client, "[SM] You cannot target this player.");
		return Plugin_Handled;
	}
	
	/*
	new ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	
	new String:target_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, target_id, MAX_COMMUNITYID_LENGTH);
	*/

	new ply_id = user_id[client];
	new target_id = user_id[target];
	
	new String:cmd[128];
	GetCmdArg(0, cmd, sizeof(cmd));
	
	GetReview(client, ply_id, target_id, StrEqual(cmd, "sm_up") ?	1 : 0, message[strlen(arg) + 1]);
	return Plugin_Handled;
}

public GetReview(client, ply_id, target_id, type, const String:note2[])
{
	new String:escped[501];
	
	new Handle:review = CreateDataPack();
	
	WritePackCell(review, ply_id);
	WritePackCell(review, target_id);
	WritePackCell(review, type);
	
	SQL_EscapeString(hDatabase, note2, escped, 501);
	WritePackString(review, escped);
	WritePackCell(review, client);
	
	new String:query[600];
	Format(query, sizeof(query), QRY_REP[0], ply_id, target_id);
	SQL_TQuery(hDatabase, QuerySelectReview, query, review);
}

public QuerySelectReview(Handle:owner, Handle:hndl, const String:error[], any:review)
{
	if (hndl == INVALID_HANDLE) { return; }
	
	ResetPack(review);
	
	new String:query[600];
	
	new ply_id = ReadPackCell(review);	
	new target_id = ReadPackCell(review);	
	new type = ReadPackCell(review);

	new String:note[501];
	ReadPackString(review, note, 501);
	new client = ReadPackCell(review);
	
	if (SQL_GetRowCount(hndl) == 0)
	{
		PrintToChat(client, "[SM] Your review has been saved.");
		Format(query, sizeof(query), QRY_REP[1], ply_id, target_id, type, note);
		SQL_TQuery(hDatabase, QRY_NONE, query);	
	}
	else
	{
		PrintToChat(client, "[SM] You can only write 1 review on the same person once every 24 hours.");
		PrintToChat(client, "[SM] If you wish to edit your previous review, go to their forum profile.");
	}
}