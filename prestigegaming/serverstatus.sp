//Map Status
new const String:QRY_MAP[2][] =
{
	"INSERT INTO pg_maps (map_name) VALUES ('$s')",
	"SELECT map_id FROM pg_maps WHERE map_name='%s'"
}

//Server Status
new const String:QRY_SERVER[3][] =
{
	"INSERT INTO pg_servers (server_ip, server_port, server_name, server_slots, server_count, server_map, server_users) VALUES ('%s', %i, '%s', %i, %i, '%s', '%s')",
	"UPDATE pg_servers SET server_ip='%s',server_port='%i',server_name='%s',server_slots='%i',server_count='%i',server_map='%s',server_users='%s' WHERE server_id='%i'",
	"SELECT server_id FROM pg_servers WHERE server_ip='%s'"
}
/* Format: string, int, string, int, int, string, string, */

public StartServerStatus()
{
	CreateTimer(SERVER_INITIAL_WAIT, Timer_AllowPlayerConnections);
}

public Action Timer_AllowPlayerConnections(Handle timer)
{
	//GetInitialStatus(); we will do this after getting the map status now...
	InitialMapStatus();
}

public InitialMapStatus()
{
	new String:Current_Map[255];
	GetCurrentMap(Current_Map, 255);

	new String:Query[512];

	Format(Query, sizeof(Query), QRY_MAP[2],Current_Map);
	SQL_TQuery(hDatabase, InitialMapStatusCallback, Query);
}

public InitialMapStatusCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("INITIALMAPSTATUSCALLBACK | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		SQL_FetchInt(hndl, 1, map_id, sizeof(map_id));
		GetInitialStatus(); // Now that we have the map id we can get the server information
	}
	else
	{
		new String:Map[255];
		GetCurrentMap(Map, 255);

		new String:Query[512];
		Format(Query, sizeof(Query), QRY_MAP[1], Map);
		SQL_TQuery(hDatabase, CreateMapCallback, Query);
	}
}

public CreateMapCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("CREATEMAPCALLBACK | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		SQL_FetchInt(hndl, 1, map_id, sizeof(map_id));
		GetInitialStatus(); //Now that we have created a map id for the current map we can set up the server status
	}
}

public GetInitialStatus()
{
	new String:Serverip[16];
	Format(Serverip. sizeof(Serverip), "%s", server_ip);

	new String:Query[512];

	Format(Query, sizeof(Query), QRY_SERVER[3],Server_Ip);
	SQL_TQuery(hDatabase, InitialStatusCallback, Query);
}

public InitialStatusCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("INITIALSTATUSCALLBACK | ERROR | - %s - %s", error, data);
	}

	if (SQL_FetchRow(hndl))
	{
		SQL_FetchInt(hndl, 1, server_id, sizeof(server_id));
		UpdateStatus = CreateTimer(SERVER_GENERAL_WAIT, Timer_UpdateStatus, _, TIMER_REPEAT)
	}
	else
	{
		CreateServerStatus();
	}
}

public CreateServerStatus()
{
	new String:Server_Ip[16];
	Format(Server_Ip, sizeof(Server_Ip), "%s", server_ip);
	new Server_Port = 27015;
	new String:Server_Name[255];
	GetConVarString(FindConVar("hostname"), Server_Name, sizeof(Server_Name));
	new Server_Slots = GetMaxHumanPlayers();
	new Server_Count = 0;
	new String:Server_Users[1024];
	new bool:firstMember = true;

	for (new i = 0; i < MAXPLAYERS; i++)
	{
		if (IsValidClient(i))
		{
			Server_Count = Server_Count + 1;
			new AdminID:id = GetUserAdmin(i);
			if (id == INVALID_ADMIN_ID)
			{
				continue;
			}
			if (!CheckCommandAccess(i, "member", ADMFLAG_CUSTOM6, true) && !CheckCommandAccess(i, "junior", ADMFLAG_CUSTOM5, true) && !CheckCommandAccess(i, "recruit", ADMFLAG_CUSTOM4, true))
			{
				continue;
			}
			new admin_user_id = user_id[i];
			new String:admin_user_id_str[16];
			IntToString(admin_user_id, admin_user_id_str, sizeof(admin_user_id_str));
			new String:base[1024];
			Format(base, sizeof(base), "%s", Server_Users);
			if(firstMember)
			{
				firstMember = false;
				Format(Server_Users, sizeof(Server_Users), "%s", admin_user_id_str);
			}
			else
			{
				Format(Server_Users, sizeof(Server_Users), "%s,%s", base, admin_user_id_str);
			}
		}
	}

	new String:Server_Map[255];
	GetCurrentMap(Server_Map, 255);

	new String:Query[2048];
	Format(Query, sizeof(Query), QRY_SERVER[1], Server_Ip, Server_Port, Server_Name, Server_Slots, Server_Count, Server_Map, Server_Users);
	SQL_TQuery(hDatabase, CreateStatusCallback, Query);
}

public CreateStatusCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("CREATESTATUSCALLBACK | ERROR | - %s - %s", error, data);
	}

	if (SQL_FetchRow(hndl))
	{
		SQL_FetchInt(hndl, 1, server_id, sizeof(server_id));
		UpdateStatus = CreateTimer(SERVER_GENERAL_WAIT, Timer_UpdateStatus, _, TIMER_REPEAT)
	}
}

public Action Timer_UpdateStatus(Handle timer)
{
	UpdateStatus();
	return Plugin_Continue;
}

public UpdateStatus()
{
	new String:update_ip[16];
	Format(update_ip, sizeof(update_ip), "%s", server_ip);
	new update_port = 27015;
	new String:update_name[255];
	GetConVarString(FindConVar("hostname"), update_name, sizeof(update_name));
	new update_slots = GetMaxHumanPlayers();
	new update_count = 0;
	new String:update_users[1024];
	new bool:firstMember = true;

	for(new i = 0; i < MAXPLAYERS; i++)
	{
		if(IsValidClient(i))
		{
			update_count = update_count + 1;
			new AdminId:id = GetUserAdmin(i);
			if(id == INVALID_ADMIN_ID)
			{
				continue;
			}
			if(!CheckCommandAccess(i, "member", ADMFLAG_CUSTOM6, true) && !CheckCommandAccess(i, "junior", ADMFLAG_CUSTOM5, true) && !CheckCommandAccess(i, "recruit", ADMFLAG_CUSTOM4, true))
			{
				continue;
			}
			new admin_user_id = user_id[i];
			new String:admin_user_id_str[16];
			IntToString(admin_user_id, admin_user_id_str, sizeof(admin_user_id_str));
			new String:base[1024];
			Format(base, sizeof(base), "%s", update_users);
			if(firstMember)
			{
				firstMember = false;
				Format(update_users, sizeof(update_users), "%s", admin_user_id_str);
			}
			else
			{
				Format(update_users, sizeof(update_users), "%s,%s", base, admin_user_id_str);
			}

		}
	}

	new String:update_map[255];
	GetCurrentMap(update_map, 255);

	new String:query[2048];
	Format(query, sizeof(query), QRY_SERVER[2], update_ip, update_port, update_name, update_slots, update_count, update_map, update_users, server_id);
	SQL_TQuery(hDatabase, QRY_NONE, query);
}
