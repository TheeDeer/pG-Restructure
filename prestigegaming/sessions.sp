//Going to place these variables here for now, when im done making this ill move them over to defines
new bool:SessionStarted[MAXPLAYERS + 1] = { false, ... };

new player_id[MAXPLAYERS + 1];
new alias_id[MAXPLAYERS + 1];
new session_id[MAXPLAYERS + 1];

new String:date;

new time_active[MAXPLAYERS + 1];
new time_inactive[MAXPLAYERS + 1];

new player_kills[MAXPLAYERS + 1];
new player_deaths[MAXPLAYERS + 1];
new player_assists[MAXPLAYERS + 1];

new const String:GET_PLAYERID[] = "SELECT player_id FROM pg_player WHERE player_steamid='%s'";
new const String:ADD_PLAYER[] = "INSERT INTO pg_player (player_steamid) VALUES (%s)";

new const String:CREATE_SESSION[] = "INSERT INTO pg_sessions (server_id,player_id,map_id,date,time_active,time_inactive,kills,deaths,assists) VALUES (%i,%i,%i,%s,%i,%i,%i,%i,%i)";
new const String:UPDATE_SESSION[] = "UPDATE pg_sessions SET session_active=%i,session_idle=%i,session_kills=%i,session_deaths=%i,session_assists=%i WHERE session_id=%i";

new const String:CREATE_IP[] = "INSERT INTO	pg_player_id (player_id,session_id,ip_addr) VALUES (%i,%i,%s)";

new const String:GET_ALIASID[] = "SELECT alias_id FROM pg_player_alias WHERE alias_title='%s'";
new const String:ADD_ALIAS[] = "INSERT INTO pg_player_alias (alias_title) VALUES (%s)";

public StartSessions()
{
	HookEvent("player_death", Session_PlayerDeath, EventHookMode_Post);

	pg_activity_type = CreateConVar("pg_activity_type", "1", "When should sessions be updated? (1 = per round, 2 = timer, 3 = per death");
}

//functions
public StartUser(client);
{
	if (client == 0 || !IsClientInGame(client) || !IsPlayerAlive(client))
	{
		return;
	}

	if(SessionStarted[client])
	{
		return;
	}

	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

	new String:Query[512];

	Format(Query, sizeof(Query), GET_PLAYERID,ply_id);
	SQL_TQuery(hDatabase, SessionStartCallback, Query, client);
}

public StartSession(client)
{
	if (client == 0 || !IsClientInGame(client) || !IsPlayerAlive(client))
	{
		return;
	}

	if(SessionStarted[client])
	{
		return;
	}

	new String:Query[512];

	Format(Query, sizeof(Query), CREATE_SESSION,server_id,player_id[client],map_id,date,time_active[client],time_inactive[client],player_kills[client],player_deaths[client],player_assists[client]);
	SQL_TQuery(hDatabase, SessionCreateCallback, Query, client);
}

public EndSession(client);
{
	if (client == 0 || !IsClientInGame(client) || !IsPlayerAlive(client))
	{
		return;
	}

	if(SessionStarted[client])
	{
		return;
	}

	new Query[512];

	Format(Query, sizeof(Query), UPDATE_SESSION,time_active[client],time_inactive[client],player_kills[client],player_deaths[client],player_assists[client]);
	SQL_TQuery(hDatabase, None, Query, client);

	SessionStarted[client] = false;
}

//Callbacks
public SessionStartCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{

	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("SessionStartCallback | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		new id[255];
		SQL_FetchInt(hndl, 1, id, sizeof(id));
		player_id[client] = id;

		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);

		new String:Query[512];

		Format(Query, sizeof(Query), GET_ALIASID,ply_name);
		SQL_TQuery(hDatabase, GetUserAliasCallback, Query, client);
	}
	else
	{
		new String:Query[512];

		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

		Format(Query, sizeof(Query), ADD_PLAYER,ply_id);
		SQL_TQuery(hDatabase, CreatePlayerCallback, Query, client);
	}
}


public GetUserAliasCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{

	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("GetUserAliasCallback | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		new id[255];
		SQL_FetchInt(hndl, 1, id, sizeof(id));
		alias_id[client] = id;

		StartSession(client);
	}
	else
	{
		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);

		new Query[512];

		Format(Query, sizeof(Query), ADD_ALIAS,ply_name);
		SQL_TQuery(hDatabase, CreateAliasCallback, Query, client);
	}
}

public CreatePlayerCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{

	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("CreatePlayerCallback | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		new id[255];
		SQL_FetchInt(hndl, 1, id, sizeof(id));
		player_id[client] = id;

		new String:ply_name[36];
		GetClientName(client, ply_name, sizeof(ply_name));
		SQL_EscapeString(hDatabase, ply_name, ply_name, 255);

		new Query[512];

		Format(Query, sizeof(Query), ADD_ALIAS,ply_name);
		SQL_TQuery(hDatabase, CreateAliasCallback, Query, client);
	}
	else
	{
		PrintToServer("CreatePlayerCallback | Did not work right...");
	}
}

public CreateAliasCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{

	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("CreateAliasCallback | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		new id[255];
		SQL_FetchInt(hndl, 1, id, sizeof(id));

		alias_id[client] = id;

		StartSession(client);
	}
}

public SessionCreateCallback(Handle:Owner, Handle:hndl, const String:error[], any:data)
{

	if (hndl == INVALID_HANDLE)
	{
		PrintToServer("SessionCreateCallback | ERROR | - %s - %s", error, data);
	}

	if(SQL_FetchRow(hndl))
	{
		new id[255];
		SQL_FetchInt(hndl, 1, id, sizeof(id));

		session_id[client] = id;

		new String:ply_ip[24];
		GetClientIP(client, ply_ip, sizeof(ply_ip));

		new Query[512];

		Format(Query, sizeof(Query), CREATE_IP,player_id[client],session_id[client],ply_ip);
		SQL_TQuery(hDatabase, None, Query, client);


		SessionStarted[client] = true;
		//Do other stuff here later (timers and stuff)
	}
}


//events
public Action:Session_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	if (!IsValidClient(attacker)) { return Plugin_Continue; }
	
	new victim = GetClientOfUserId(GetEventInt(event, "userid"));
	if (!IsValidClient(victim)) { return Plugin_Continue; }

	new assister = GetClientOfUserId(GetEventInt(event, "assister"));
	if (!IsValidClient(assister)) { return Plugin_Continue; }

}