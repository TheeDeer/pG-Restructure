/*new const String:Procedure[3][] =
{
	"DELIMITER //
	CREATE PROCEDURE pg_stats_top
	(IN serverip varchar(16))
	BEGIN
		SET @rownum = 0;SELECT @rownum:=@rownum+1 `rank`, T.*, ((1000+T.kills) + (T.assists / 2) - T.deaths) AS points FROM ( SELECT `attacker_id` AS individual, pg_stats.server_ip, (SELECT ply_name FROM `pg_logs` WHERE ply_id=individual ORDER BY log_id DESC LIMIT 0,1) AS name, (SELECT SUM( CASE WHEN `attacker_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE server_ip = serverip) AS kills, (SELECT SUM( CASE WHEN `assist_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE server_ip = serverip) AS assists, (SELECT SUM( CASE WHEN `victim_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE server_ip = serverip) AS deaths FROM `pg_stats` LEFT OUTER JOIN `pg_logs` ON pg_logs.ply_id=pg_stats.attacker_id WHERE pg_stats.server_ip = serverip GROUP BY `attacker_id`) as T ORDER BY `points` DESC LIMIT 0, 50;
	END //
	DELIMITER ;",
	"DELIMITER //
	CREATE PROCEDURE pg_stats_client
	(IN param BIGINT(64), IN serverip varchar(16))
	BEGIN
		SET @rownum = 0;SELECT * FROM ( SELECT @rownum:=@rownum+1 `rank`, T.*, ((1000+T.kills) + (T.assists / 2) - T.deaths) AS points FROM ( SELECT `attacker_id` AS individual, pg_stats.server_ip, (SELECT ply_name FROM `pg_logs` WHERE ply_id=individual ORDER BY log_id DESC LIMIT 0,1) AS name, (SELECT SUM( CASE WHEN `attacker_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE pg_stats.server_ip = serverip) AS kills, (SELECT SUM( CASE WHEN `assist_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE pg_stats.server_ip = serverip) AS assists, (SELECT SUM( CASE WHEN `victim_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE pg_stats.server_ip = serverip) AS deaths FROM `pg_stats` LEFT OUTER JOIN `pg_logs` ON pg_logs.ply_id=pg_stats.attacker_id WHERE pg_stats.server_ip = serverip GROUP BY `attacker_id` ) as T ORDER BY `points` DESC) as B WHERE individual = param LIMIT 0, 1;
	END //
	DELIMITER ;",
	"DELIMITER //
	CREATE PROCEDURE pg_stats_next
	(IN param INT(9), IN serverip varchar(16))
	BEGIN
		SET @rownum = 0;SELECT * FROM ( SELECT @rownum:=@rownum+1 `rank`, T.*, ((1000+T.kills) + (T.assists / 2) - T.deaths) AS points FROM ( SELECT `attacker_id` AS individual, pg_stats.server_ip, (SELECT ply_name FROM `pg_logs` WHERE ply_id=individual ORDER BY log_id DESC LIMIT 0,1) AS name, (SELECT SUM( CASE WHEN `attacker_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE pg_stats.server_ip = serverip) AS kills, (SELECT SUM( CASE WHEN `assist_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE pg_stats.server_ip = serverip) AS assists, (SELECT SUM( CASE WHEN `victim_id` = individual THEN 1 ELSE 0 END ) FROM `pg_stats` WHERE pg_stats.server_ip = serverip) AS deaths FROM `pg_stats` LEFT OUTER JOIN `pg_logs` ON pg_logs.ply_id=pg_stats.attacker_id WHERE pg_stats.server_ip = serverip GROUP BY `attacker_id` ) as T ORDER BY `points` DESC) as B WHERE rank < param ORDER BY rank DESC LIMIT 0, 1;
	END //
	DELIMITER ;"
};*/

public StartStatistics()
{
	pg_statistics 	= CreateConVar("pg_statistics", "0", "0 = Disabled, 1 = Normal, 2 = Traitors Only, 3 = Prisoners Only");
	
	RegConsoleCmd("sm_rank", Command_Rank);
	RegConsoleCmd("sm_stats", Command_Rank);
	RegConsoleCmd("sm_top", Command_Top);
	RegConsoleCmd("sm_top10", Command_Top);
	
	ply_players = CreateTrie();
	
	for (new i = 1; i <= 50; i++)
	{
		ply_top[i] = "undefined";
	}
	
	HookConVarChange(pg_statistics, OnConVarChange);
}

public OnConVarChange(Handle:hCvar, const String:oldValue[], const String:newValue[])
{
	if(GetConVarInt(pg_statistics) != 0)
	{
		GetAllClientStats();
		GetTop();
	}
}

any:GetEnemyField(String:id[], String:field[])
{
	new String:enemy[MAX_COMMUNITYID_LENGTH];
	new Handle:tmp;
	GetTrieValue(ply_players, id, tmp);
	GetTrieString(tmp, "enemy", enemy, sizeof(enemy));
	return GetField(enemy, field);
}

any:GetField(String:ply_id[], String:field[])
{
	new Handle:tmp;
	if(!GetTrieValue(ply_players, ply_id, tmp))
	{
		return 0;
	}
	new any:tmp_field;
	GetTrieValue(tmp, field, tmp_field);
	return tmp_field;
}

Float:GetPoints(String:ply_id[])
{
	new kills = GetField(ply_id, "kills");
	new assists = GetField(ply_id, "assists");
	new deaths = GetField(ply_id, "deaths");
	
	return (1000.0 + kills) + (assists / 2.0) - deaths;
}

Float:GetEnemyPoints(String:ply_id[])
{
	new String:enemy[MAX_COMMUNITYID_LENGTH];
	new Handle:tmp;
	GetTrieValue(ply_players, ply_id, tmp);
	GetTrieString(tmp, "enemy", enemy, sizeof(enemy));
	return GetPoints(enemy);
}

SaveField(String:id[], String:field[], any:value)
{
	new Handle:tmp;
	GetTrieValue(ply_players, id, tmp);
	SetTrieValue(tmp, field, value);
	SetTrieValue(ply_players, id, tmp);
}

SaveData(String:ply_id[], String:name[] = "", kills = 0, deaths = 0, assists = 0, rank = 0, String:enemy[] = "", killstreak = -1)
{
	new Handle:tmp;
	if (!GetTrieValue(ply_players, ply_id, tmp))
	{
		tmp = CreateTrie();
		SetTrieString(tmp, "name", "");
		SetTrieValue(tmp, "kills", 0);
		SetTrieValue(tmp, "deaths", 0);
		SetTrieValue(tmp, "assists", 0.0);
		SetTrieValue(tmp, "rank", 0);
		SetTrieString(tmp, "enemy", "");
		SetTrieValue(tmp, "killstreak", 0);
	}
	//if (!GetTrieValue(ply_players, enemy, tmp))
	//{
	//	GetClientStats(enemy); // Load enemy data if not already loaded
	//}
	
	if (!StrEqual(name, "")) { SetTrieString(tmp, "name", name); }
	
	new old_rank;
	GetTrieValue(tmp, "rank", old_rank);
	new String:old_enemy[MAX_COMMUNITYID_LENGTH];
	GetTrieString(tmp, "enemy", old_enemy, sizeof(old_enemy));
	if (kills > 0) { SetTrieValue(tmp, "kills", kills); }
	if (deaths > 0) { SetTrieValue(tmp, "deaths", deaths); }
	if (assists > 0) { SetTrieValue(tmp, "assists", assists); }
	if (rank != 0) { SetTrieValue(tmp, "rank", rank); }
	
	if (!StrEqual(enemy, "")) { SetTrieString(tmp, "enemy", enemy); }
	
	// Always update killstreak, as it can go back to 0
	if (killstreak > -1) { SetTrieValue(tmp, "killstreak", killstreak); } 
	
	SetTrieValue(ply_players, ply_id, tmp);

	if (rank <= 50 && rank != 0)
	{
		// Client is in the top 50, so put them in the top array
		new String:tmp_id[MAX_COMMUNITYID_LENGTH];
		strcopy(tmp_id, sizeof(tmp_id), ply_id);
		ply_top[rank] = tmp_id;
	}
	
	if (old_rank != 0 && rank < old_rank && rank != 0)
	{
		GetTrieString(tmp, "enemy", enemy, MAX_COMMUNITYID_LENGTH);
		new old_enemy_client = GetClientFromCommunityId(old_enemy);
		if(IsValidClient(old_enemy_client))
		{
			// Force enemy update because we beat them
			GetClientStats(old_enemy);
		}
		new client = GetClientFromCommunityId(ply_id);
		if(rank >= 50)
		{
			CPrintToChat(client, "{red}Rank progress: {default}Ranked up!");
			new i = GetClientFromCommunityId(enemy);
			if (IsValidClient(i))
			{
				CPrintToChat(i, "{red}Rank progress: {default}Ranked down!");
			}
		}
		else
		{
			new String:enemy2[MAX_COMMUNITYID_LENGTH], String:enemy_name[256];
			new Handle:tmp1, Handle:tmp2;
			GetTrieValue(ply_players, ply_id, tmp1);
			GetTrieString(tmp1, "enemy", enemy2, sizeof(enemy2));
			GetTrieValue(ply_players, enemy2, tmp2);
			GetTrieString(tmp2, "name", enemy_name, sizeof(enemy_name));
			CPrintToChatAll("{red}%N {default}has progressed to rank {lightgreen}#%i (beaten: %s).", client, rank, enemy_name);
		}
	}
}

public GetClientFromCommunityId(String:id[])
{
	for (new i = 1; i <= MAXPLAYERS; i++)
	{
		if (!IsValidClient(i)) { continue; }

		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		GetClientAuthId(i, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		if (StrEqual(ply_id, id)) { return i; }
	}
	return 0;
}

public GetAllClientStats()
{
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	for (new i = 1; i <= MAXPLAYERS; i++)
	{
		if (!IsValidClient(i)) { continue; }
		
		GetClientAuthId(i, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
		
		GetClientStats(ply_id);
	}
}

public GetClientStats(String:ply_id[])
{
	new Handle:dataPack = CreateDataPack();
	WritePackString(dataPack, ply_id);
	
	new String:query[6500];
	Format(query, sizeof(query), QRY_STATS[1], ply_id, server_ip);
	SQL_TQuery(hDatabase, QueryClientStats, query, dataPack);
}

public QueryClientStats(Handle:owner, Handle:hndl, const String:error[], Handle:dataPack)
{
	if (hndl == INVALID_HANDLE) { return; }
	
	ResetPack(dataPack);
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	ReadPackString(dataPack, ply_id, MAX_COMMUNITYID_LENGTH);
	
	if (SQL_FetchRow(hndl))
	{
		new String:ply_name[65];
		SQL_FetchString(hndl, 3, ply_name, sizeof(ply_name));
		SaveData(ply_id, ply_name, SQL_FetchInt(hndl, 4), SQL_FetchInt(hndl, 6), SQL_FetchInt(hndl, 5), SQL_FetchInt(hndl, 0));
		
		if (GetField(ply_id, "rank") != 1)
		{
			new String:query[6500];
			Format(query, sizeof(query), QRY_STATS[2], GetField(ply_id, "rank"), server_ip);
			SQL_TQuery(hDatabase, QueryTargetStats, query, dataPack);
		}
		else
		{
			CloseHandle(dataPack);
		}
		
		if (GetField(ply_id, "rank") <= 10)
		{
			new client = GetClientFromCommunityId(ply_id);
			
			if (IsValidClient(client))
			{
				// Note: It is not removed until server restart.
				new AdminId:id = GetUserAdmin(client);
				if(id == INVALID_ADMIN_ID)
				{
					new AdminId:Top10 = CreateAdmin("Top10");
					SetAdminFlag(Top10, Admin_Custom3, true);
					SetUserAdmin(client, Top10);
				}
				else
				{
					SetAdminFlag(id, Admin_Custom3, true);
				}
			}
		}
	}
	else
	{
		CloseHandle(dataPack);
		SaveData(ply_id);
	}
}

GetTop()
{
	new String:query[6500];
	Format(query, sizeof(query), QRY_STATS[0], server_ip);
	SQL_TQuery(hDatabase, QueryTopStats, query);
}

public QueryTopStats(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if (hndl == INVALID_HANDLE) { return; }

	while (SQL_FetchRow(hndl))
	{
		new String:ply_id[MAX_COMMUNITYID_LENGTH];
		SQL_FetchString(hndl, 1, ply_id, sizeof(ply_id));
		
		new String:ply_name[65];
		SQL_FetchString(hndl, 3, ply_name, sizeof(ply_name));
		
		SaveData(ply_id, ply_name, SQL_FetchInt(hndl, 4), SQL_FetchInt(hndl, 6), SQL_FetchInt(hndl, 5), SQL_FetchInt(hndl, 0));
	}
	if (SQL_MoreRows(hndl))
	{
		return;
	}
}

public QueryTargetStats(Handle:owner, Handle:hndl, const String:error[], Handle:dataPack)
{
	if (hndl == INVALID_HANDLE) { return; }
	
	ResetPack(dataPack);
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	ReadPackString(dataPack, ply_id, MAX_COMMUNITYID_LENGTH);
	CloseHandle(dataPack);
	
	if (SQL_FetchRow(hndl))
	{
		new String:row_id[MAX_COMMUNITYID_LENGTH];
		SQL_FetchString(hndl, 1, row_id, sizeof(row_id));
		
		new String:row_name[65];
		SQL_FetchString(hndl, 3, row_name, sizeof(row_name));
		
		SaveData(ply_id, "", 0, 0, 0, 0, row_id); // 0's are ignored
		SaveData(row_id, row_name, SQL_FetchInt(hndl, 4), SQL_FetchInt(hndl, 6), SQL_FetchInt(hndl, 5), SQL_FetchInt(hndl, 0));
	}
}

public GetMsgKillStreak(attacker,count)
{
	if (count >= 3)
	{
		CPrintToChatAll("{red}%N {default}is on a killing streak with {red}%i {default}kills.", attacker, count);
	}
}

public GetMsgRevenge(attacker,victim)
{
	CPrintToChatAll("{red}%N {default}got revenge on {red}%N.", attacker, victim);
}

public GetMsgDomination(attacker,victim)
{
	CPrintToChatAll("{red}%N {default}is now dominating {red}%N.", attacker, victim);
}

public GetMsgRank(client)
{
	if (!IsValidClient(client)) { return; }
	
	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(client, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);
	
	if (GetField(ply_id, "rank") == 0)
	{
		if(GetField(ply_id, "kills") != 1) { return; }
		CPrintToChat(client, "{red}Rank progress: {default}you have achieved a !rank.", client);
		PrintToChat(client, "   - Gain {lightgreen}+1 {default}points for kills.");
		PrintToChat(client, "   - Gain {lightgreen}+0.5 {default}points for assists.");
		PrintToChat(client, "   - Loose {lightgreen}-1 {default}points for death.");
		PrintToChat(client, "   - {lightgreen}Commands: {default}!rank, !top, !rank <name>.");
		PrintToChat(client, "   - The top10 players receive {lightgreen}VIP for free{default}!");
		GetClientStats(ply_id);
		return;
	}
	
	if (GetField(ply_id, "rank") == 1) { return; }
	
	new Float:points_remaining;
	points_remaining = FloatSub(FloatAdd(GetEnemyPoints(ply_id), 1.0), GetPoints(ply_id));
	
	new String:enemy[MAX_COMMUNITYID_LENGTH], String:enemy_name[256];
	new Handle:tmp, Handle:tmp2;
	GetTrieValue(ply_players, ply_id, tmp);
	GetTrieString(tmp, "enemy", enemy, sizeof(enemy));
	GetTrieValue(ply_players, enemy, tmp2);
	GetTrieString(tmp2, "name", enemy_name, sizeof(enemy_name));
	if (points_remaining > 0.0)
	{
		CPrintToChat(client, "{red}Rank progress: {lightgreen}+%i {default}points to beat %s (#%i)", RoundToFloor(points_remaining), enemy_name, GetEnemyField(ply_id, "rank"));
	}
	else
	{
		GetClientStats(ply_id);
	}
}

public Action:Command_Top (client, args)
{
	if(GetConVarInt(pg_statistics) == 0)
	{
		return Plugin_Stop;
	}
	
	new Handle:menu = CreateMenu(TopMenuHandler);
	decl String:title[100];
	Format(title, sizeof(title), "Top 50");
	SetMenuTitle(menu, title);

	// Loop through the Top 50
	for (new i = 1; i <= 50; i++) {
		new Handle:hndl = INVALID_HANDLE;
		new String:name[128];
		GetTrieValue(ply_players, ply_top[i], hndl);
		if (hndl == INVALID_HANDLE) continue;
		GetTrieString(hndl, "name", name, sizeof(name));
		new String:name_format[128];
		Format(name_format, sizeof(name_format), "%s (#%i - %i Points)", name, GetField(ply_top[i], "rank"), RoundToFloor(GetPoints(ply_top[i])));
		new String:rank_format[128];
		Format(rank_format, sizeof(rank_format), "Rank %i", i);
		AddMenuItem(menu, rank_format, name_format);
	}
	DisplayMenu(menu, client, 30); // Show menu for 30 seconds or until closed
	return Plugin_Handled;
}

public TopMenuHandler (Handle:menu, MenuAction:action, param1, param2)
{
	switch(action)
	{
		case MenuAction_Select:
		{
			DisplayRankMenu(param1, ply_top[param2+1]);
		}
		case MenuAction_Cancel:
		{
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
	return 0;
}

public Action:Command_Rank(client, args)
{
	if(GetConVarInt(pg_statistics) == 0)
	{
		return Plugin_Stop;
	}
	
	decl String:Arguments[256];
	GetCmdArgString(Arguments, sizeof(Arguments));

	decl String:arg[65];
	BreakString(Arguments, arg, sizeof(arg));
	new target;
	
	if (args == 1) { target = FindTarget(client, arg, true, false); }
	else { target = client; }

	new String:ply_id[MAX_COMMUNITYID_LENGTH];
	GetClientAuthId(target, AuthId_SteamID64, ply_id, MAX_COMMUNITYID_LENGTH);

	DisplayRankMenu(client, ply_id);
	return Plugin_Handled;
}

DisplayRankMenu(client, String:ply_id[])
{
	new Handle:menu = CreateMenu(RankMenuHandler);
	decl String:title[100];
	new Handle:hndl = INVALID_HANDLE;
	new String:name[128];
	GetTrieValue(ply_players, ply_id, hndl);
	GetTrieString(hndl, "name", name, sizeof(name));
	Format(title, sizeof(title), "%s's Rank", name);
	SetMenuTitle(menu, title);
	
	
	new String:format_rank[128];
	Format(format_rank, sizeof(format_rank), "Rank: #%i", GetField(ply_id, "rank"));
	AddMenuItem(menu, "Rank", format_rank);
	new String:format_points[128];
	Format(format_points, sizeof(format_points), "Points: %i", RoundToFloor(GetPoints(ply_id)));
	AddMenuItem(menu, "Points", format_points);
	new String:format_kills[128];
	Format(format_kills, sizeof(format_kills), "Kills: %i", GetField(ply_id, "kills"));
	AddMenuItem(menu, "Kills", format_kills);
	new String:format_deaths[128];
	Format(format_deaths, sizeof(format_deaths), "Deaths: %i", GetField(ply_id, "deaths"));
	AddMenuItem(menu, "Deaths", format_deaths);
	new String:format_assists[128];
	Format(format_assists, sizeof(format_assists), "Assists: %i", GetField(ply_id, "assists"));
	AddMenuItem(menu, "Assists", format_assists);
	new String:format_ratio[128];
	Format(format_ratio, sizeof(format_ratio), "K/D Ratio: %f", float(GetField(ply_id, "kills")) / float(GetField(ply_id, "deaths")));
	AddMenuItem(menu, "ratio", format_ratio);
	
	DisplayMenu(menu, client, 30); // Show rank for 30 seconds or until closed	
}

public RankMenuHandler(Handle:menu, MenuAction:action, param1, param2)
{
	/* Do nothing */
}