public StartEnforcer()
{
	HookEvent("player_changename", OnNameChange, EventHookMode_Post);
	HookUserMessage(GetUserMessageId("SayText2"), SayText2Hook, true);
	hTags = CreateArray(16, MAXPLAYERS + 1);
}

public validateName(client)
{
	new String:name[36];
	GetClientName(client, name, sizeof(name));
	if (StrContains(name, "'", false) >= 0 || StrContains(name, "\"", false) >= 0 || StrContains(name, "\\", false) >= 0) {
		ReplaceString(name, sizeof(name), "'", "", false);
		ReplaceString(name, sizeof(name), "\"", "", false);
		ReplaceString(name, sizeof(name), "\\", "", false);
		SetClientName(client, name);
	}
	return;
}

public CheckUserName(client) {
	if (in_name_change[client]) {
		//PrintToChatAll("%N INC Override", client);
		return;
	}
	if (IsValidClient(client)) {
		new AdminId:id = GetUserAdmin(client);
		decl String:username[MAX_NAME_LENGTH];
		GetClientName(client, username, sizeof(username));
		if (id != INVALID_ADMIN_ID)
		{
			decl String:admin_name[MAX_NAME_LENGTH];
			GetAdminUsername(id, admin_name, sizeof(admin_name));
			decl String:tag[8];
			GetArrayString(hTags, client, tag, sizeof(tag));
			if (StrEqual(tag, "")) {
				//PrintToChatAll("%N No Tag", client);
				// Still check for DS
				if (StrContains(username, "-pg-", false) == 0 || StrContains(username, "[pG]", false) == 0 || StrContains(username, "=[pG]=") == 0) {
					KickClient(client, "Remove the pG tags - visit www.prestige-gaming.org with any questions.");
				}
				// Doesn't have a tag to match
				return;
			}
			if (StrContains(username, tag, false) == 0 && StrContains(username, admin_name, false) == (strlen(tag) + 1))
			{
				//PrintToChatAll("%N Proper Tag %s", client, tag);
				// Already wearing the tag and the proper username
				return;
			}
			new String:new_name[MAX_NAME_LENGTH];
			Format(new_name, sizeof(new_name), "%s %s", tag, admin_name);
			PrintToServer("[TagEnforcer] %N Failed Name Check (%s)", client, new_name);
			in_name_change[client] = true;
			SetClientName(client, new_name);
			in_name_change[client] = false;
		} else {
			//PrintToChatAll("No Admin Detected %N", client);
			if (StrContains(username, "-pg-", false) == 0 || StrContains(username, "[pG]", false) == 0 || StrContains(username, "=[pG]=") == 0) {
				KickClient(client, "Remove the pG tags - visit www.prestige-gaming.org with any questions.");
			}
		}
	}
}

public Action:CheckUserNameTimer(Handle timer, client) {
	CheckUserName(client);
}

public Action:OnNameChange(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	//PrintToChatAll("%N changed name", client);

	//KickClient(client, "Changed name while on server - Please reconnect");
	CreateTimer(0.1, CheckUserNameTimer, client);

	return Plugin_Continue;
}

ShouldHideNameChange(int client) {
	char tag[8];
	GetArrayString(hTags, client, tag, sizeof(tag));
	return !StrEqual(tag, "");
}

public Action SayText2Hook(UserMsg msg_id, Handle bf, const int[] players,
						   int playersNum, bool reliable, bool init) {
	
	int client;
	char msg[25];
	
	// message is either a protobuf (e.g., CS:GO) or bitbuffer (e.g., TF2)
	if (GetUserMessageType() == UM_Protobuf) {
		client = PbReadInt(bf, "ent_idx");
		PbReadString(bf, "msg_name", msg, sizeof msg);
	} else {
		client = BfReadEntity(bf);
		BfReadString(bf, msg, sizeof(msg));
	}
	
	// hide messages about name changes for clients that should have a tag
	if (StrContains(msg, "Name_Change") != -1 && IsValidClient(client) && ShouldHideNameChange(client)) {
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}