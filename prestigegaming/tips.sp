public StartTips()
{
	g_hMessages = CreateKeyValues("prestigegaming_tips");
	if(!FileExists(FILE_PATH))
	{
		return;
	}
	FileToKeyValues(g_hMessages, FILE_PATH);
	if(KvJumpToKey(g_hMessages, "Messages")) { KvGotoFirstSubKey(g_hMessages); }
	CreateTimer(60.0, GetTipMsg, _, TIMER_REPEAT);
}

public Action:GetTipMsg(Handle: timer)
{
	if(!KvGotoNextKey(g_hMessages))
	{
		KvGoBack(g_hMessages);
		KvGotoFirstSubKey(g_hMessages);
	}
	for(new i = 1 ; i < MaxClients; i++)
	{
		if(IsValidClient(i))
		{
			new String:tipMsg[256];
			KvGetString(g_hMessages, "tip", tipMsg, sizeof(tipMsg), "We are recruiting, apply at www.prestige-gaming.org");
			CPrintToChat(i,"{green}Tips: %s", tipMsg);
		}
	}
}